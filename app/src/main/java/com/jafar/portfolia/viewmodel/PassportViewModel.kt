package com.jafar.portfolia.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.jafar.portfolia.database.Entity.*
import com.jafar.portfolia.database.PassportEntity.*
import com.jafar.portfolia.repository.PassportRepository
import kotlinx.coroutines.launch

class PassportViewModel (val app: Application,
val repository: PassportRepository):AndroidViewModel(app) {

    // reading
    fun getReading()=repository.getReading()

    fun saveReading(readingQ: ReadingQ)=viewModelScope.launch {
        repository.insertReading(readingQ)
    }
    fun deletingRead()=viewModelScope.launch {
        repository.deleteReading()
    }
    // listening
    fun getListening()=repository.getListening()

    fun saveListening(listeningQ: ListeningQ)=viewModelScope.launch {
        repository.insertListening(listeningQ)
    }
    fun deletingListening()=viewModelScope.launch {
        repository.deleteListening()
    }
    // writing
    fun getWriting()=repository.getWritingQ()

    fun saveWriting(writingQ: WritingQ)=viewModelScope.launch {
        repository.insertWriting(writingQ)
    }
    fun deletingWriting()=viewModelScope.launch {
        repository.deleteWriting()
    }
    // spoken interaction
    fun getSpokenInteraction()=repository.getSpokenInteraction()

    fun saveSpokenInter(spokenInterQ: SpokenInterQ)=viewModelScope.launch {
        repository.insertSpokenInteraction(spokenInterQ)
    }
    fun deletingSpokenInter()=viewModelScope.launch {
        repository.deleteSpokenInteraction()
    }
    // spoken production
    fun getSpokenProduction()=repository.getSpokenProdiction()

    fun saveSpokenproduction(spokenProductionQ: SpokenProductionQ)=viewModelScope.launch {
        repository.insertSpokenProduction(spokenProductionQ)
    }
    fun deletingSpokenProd()=viewModelScope.launch {
        repository.deleteSpokenProduction()
    }


    // primary edu
    fun getPrimary_edu()=repository.getPrimary_edu()

    fun savePrimary_edu(primaryEdu: Primary_edu)=viewModelScope.launch {
        repository.insertPrimary_edu(primaryEdu)
    }
    // secondary edu
    fun getSecondary_edu()=repository.getSecondary_edu()

    fun saveSecondary_edu(secondaryEdu: Secondary_edu)=viewModelScope.launch {
        repository.insertSecondary_edu(secondaryEdu)
    }
    // higher edu
    fun getHigher_edu()=repository.getHigher_edu()

    fun saveHigher_edu(higherEdu: higher_edu)=viewModelScope.launch {
        repository.insertHigher_edu(higherEdu)
    }

    // Extra fragment
    fun getLanguageWork()=repository.getLanguageWork()

    fun saveLanguageWork(languageWork: Language_work)=viewModelScope.launch {
        repository.insertLanguageWork(languageWork)
    }

    fun getLinguisticStudy()=repository.getLinguisticStudy()

    fun saveLinguisticStudy(linguisticStudy: Linguistic_study)=viewModelScope.launch {
        repository.insertLinguistic_study(linguisticStudy)
    }

    fun getTravelkLiving()=repository.getTravvelLiving()

    fun saveTravelLiving(travellingLiving: Travelling_living)=
        viewModelScope.launch { repository.insertTravellingLiving(travellingLiving) }


}