package com.jafar.portfolia.database.PassportEntity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "travelling_living")
data class Travelling_living(
    val language:String,
    val details:String,
    val from:String,
    val to:String
){
    @PrimaryKey(autoGenerate = true)
    var id:Int?=null
}