package com.jafar.portfolia.database.Entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "spoken_inter")
class SpokenInterQ (val quiz:String,
                    val answer:String){
    @PrimaryKey(autoGenerate = true)
    var id:Int?=null
}