package com.jafar.portfolia.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jafar.portfolia.database.Dao.PassportDao
import com.jafar.portfolia.database.Dao.RegisterDao
import com.jafar.portfolia.database.Entity.*
import com.jafar.portfolia.database.PassportEntity.*

@Database(entities = [
         RegistrationEntity::class,
         Certificate::class,
         Writing::class,
         MyAims::class,
         MyDiary::class,
         SelfAssesment::class,
         Primary_edu::class,Secondary_edu::class,higher_edu::class,
         Language_work::class,Linguistic_study::class,Travelling_living::class,
         ReadingQ::class,ListeningQ::class,WritingQ::class,
         SpokenProductionQ::class,SpokenInterQ::class],version = 1,exportSchema = false)
abstract class MainDatabase : RoomDatabase() {

    abstract fun getRegister() :RegisterDao
    abstract fun getPassport(): PassportDao

    companion object {
        @Volatile
        private var instance: MainDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: createDatabase(context).also { instance = it }
        }

        private fun createDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                MainDatabase::class.java,
                "portfolia_db.db"
            ).build()
    }
}
