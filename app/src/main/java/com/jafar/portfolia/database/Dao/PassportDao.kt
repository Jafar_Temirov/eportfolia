package com.jafar.portfolia.database.Dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.jafar.portfolia.database.Entity.*
import com.jafar.portfolia.database.PassportEntity.*
import com.jafar.portfolia.model.Reading

@Dao
interface PassportDao {

    @Query("SELECT * FROM primary_edu ORDER BY id DESC")
    fun getPrimary_edu() : LiveData<List<Primary_edu>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPrimary_edu(primaryEdu: Primary_edu): Long

    @Query("SELECT * FROM secondary_edu ORDER BY id DESC")
    fun getSecond_edu() : LiveData<List<Secondary_edu>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSecond_edu(secondaryEdu: Secondary_edu): Long

    @Query("SELECT * FROM higher_edu ORDER BY id DESC")
    fun getHigher_edu() : LiveData<List<higher_edu>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertHigher_edu(higherEdu: higher_edu): Long

    // My language learning
    @Query("SELECT * FROM language_work ORDER BY id DESC")
    fun getLanguage_work():LiveData<List<Language_work>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLanguage_work(languageWork: Language_work): Long

    @Query("SELECT * FROM linguistic_study ORDER BY id DESC")
    fun getLinguisticStudy():LiveData<List<Linguistic_study>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLinguistic_study(linguisticStudy: Linguistic_study): Long

    @Query("SELECT * FROM travelling_living ORDER BY id DESC")
    fun getTravellingLiving():LiveData<List<Travelling_living>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTravellingLiving(travellingLiving: Travelling_living): Long
 // reading
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertReading(reading: ReadingQ): Long

    @Query("SELECT *FROM reading_quiz ")
    fun getReading() :LiveData<List<ReadingQ>>

    @Query("DELETE FROM reading_quiz")
    suspend fun deleteReading()
// listening
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertListening(listeningQ: ListeningQ): Long

    @Query("SELECT *FROM listening_quiz ")
    fun getListening() :LiveData<List<ListeningQ>>

    @Query("DELETE FROM listening_quiz")
    suspend fun deleteListening()
// writing
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWriting(writing: WritingQ): Long

    @Query("SELECT *FROM writing_quiz ")
    fun getWriting() :LiveData<List<WritingQ>>

    @Query("DELETE FROM writing_quiz")
    suspend fun deleteWriting()
// Spoken interaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSpokenInteraction(spokenInterQ: SpokenInterQ): Long

    @Query("SELECT *FROM spoken_inter ")
    fun getSpokenInter() :LiveData<List<SpokenInterQ>>

    @Query("DELETE FROM spoken_inter")
    suspend fun deleteSpokenInter()
// spoken production
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSpokenProduction(spokenProductionQ: SpokenProductionQ): Long

    @Query("SELECT *FROM spoken_prod ")
    fun getSpokenProd() :LiveData<List<SpokenProductionQ>>

    @Query("DELETE FROM spoken_prod")
    suspend fun deleteSpokenProd()


}