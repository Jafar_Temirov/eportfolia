package com.jafar.portfolia.util.myachievement

import com.jafar.portfolia.model.Reading

object ReadingQuestion{

    fun getReadingQuiz() :ArrayList<Reading>{
        val questionList=ArrayList<Reading>()

        val que1= Reading(
            1,
            "I can read straightforward factual texts on subjects related to my field of interest with a reasonable level of understanding",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que1)

        val que2= Reading(
            2,
            "I can recognise significant points in straightforward newspaper articles on familiar subjects",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que2)

        val que3= Reading(
            3,
            "I can identify the main conclusions in clearly signalled argumentative texts related to my academic or professional field",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que3)

        val que4= Reading(
            4,
            "I can understand the description of events, feelings and wishes in personal letters and e-mails well enough to correspond with a pen friend",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que4)

        val que5= Reading(
            5,
            "I can find and understand relevant information in everyday material, such as standard letters, brochures and short official documents",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que5)

        val que6= Reading(
            6,
            "I can understand clearly written straightforward instructions (e.g., for using a piece of equipment, answering questions in an exam, installing computer software, preparing food)",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que6)

        val que7= Reading(
            7,
            "I can scan longer texts in my field in order to locate desired information, and gather information from different parts of a text, or from different texts in order to fulfil a specific task",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que7)

        val que8= Reading(
            8,
            "I can follow the plot of clearly structured narratives and modern literary texts",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que8)

        val que9= Reading(
            9,
            "I can skim short texts (e.g. news summaries) and find relevant facts and information (e.g. who has done what and where)",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que9)
        // Second step
        val que10= Reading(
            10,
            " I can quickly scan through long and complex texts on a variety of topics in my field to locate relevant details",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que10)

        val que11= Reading(
            11,
            "  I can read correspondence relating to my field of interest and readily grasp the essential meaning",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que11)
        val que12= Reading(
            12,
            "I can obtain information, ideas and opinions from highly specialised sources within my academic or professional field",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que12)

        val que13= Reading(
            13,
            "I can understand articles on specialised topics using a dictionary and other appropriate reference resources",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que13)

        val que14= Reading(
            14,
            "I can quickly identify the content and relevance of news items, articles and reports on a wide range of professional topics, deciding whether closer study is worthwhile",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que14)

        val que15= Reading(
            15,
            "I can understand articles and reports concerned with contemporary problems in which the writers adopt particular stances or viewpoints",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que15)

        val que16= Reading(
            16,
            "I can understand lengthy complex instructions in my field, including details on conditions or warnings, provided I can reread difficult sections ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que16)
        val que17= Reading(
            17,
            "I can read most modern literary texts (e.g. novels, short stories, poems, plays) with little difficulty when I have got used to the author’s style",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que17)

        // finished the second ones.
        val que18= Reading(
            18,
            "I can scan relatively quickly through books and articles within my field of interest and assess their relevant to my needs",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que18)

        val que19= Reading(
            19,
            "I can understand in detail highly specialised texts in my own academic or professional field, such as research reports and abstracts, though I may want time to reread them",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que19)
        val que20= Reading(
            21,
            "I can understand a wide range of long and complex texts in which stated opinions and implied points of view are discussed",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que20)
        val que21= Reading(
            21,
            "I can understand formal letters connected or unconnected with my field if I can occasionally check with a dictionary",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que21)
        val que22= Reading(
            22,
            "I can read contemporary literary texts with no difficulty and with appreciation of implicit meanings and ideas ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que22)
        val que23= Reading(
            23,
            "I can read extensively for pleasure without using a dictionary unless I want to check precise meaning, usage or pronunciation",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que23)
        val que24= Reading(
            24,
            "I can understand detailed and complex instructions for a new machine or procedure, whether or not the instructions relate to my own area of speciality, provided I can reread difficult sections",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que24)
        return questionList
    }
}