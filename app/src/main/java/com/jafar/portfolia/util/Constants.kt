package com.jafar.portfolia.util

class Constants {

    companion object{
        const val REQUEST_CODE_IMAGE=100
        const val CAMERA_PERM_CODE = 101
        const val CAMERA_REQUEST_CODE = 102
        const val GALLERY_REQUEST_CODE = 105

        const val PDF_SELECTION_CODE=121

        const val IS_REGISTERED="is_first"
        const val IS_SELF_ASSESSED="is_self_assessment"

        const val READING="reading"
        const val READING_TITLE="reading_title"

        const val LISTENING="listening"
        const val LISTENING_TITLE="listening_title"
        const val WRITING="writing"
        const val WRITING_TITLE="writing_title"
        const val SPOKEN_INTERACTION="spoken_interaction"
        const val SPOKEN_INTERACTION_TITLE="spoken_interaction_title"
        const val SPOKEN_PRODUCTION="spoken_production"
        const val SPOKEN_PRODUCTION_TITLE="spoken_production_title"
    }
}