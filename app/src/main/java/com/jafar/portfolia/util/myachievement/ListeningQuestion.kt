package com.jafar.portfolia.util.myachievement

import com.jafar.portfolia.model.Listening
import com.jafar.portfolia.model.Reading

object ListeningQuestion{

    fun getListeningQuiz() :ArrayList<Listening>{
        val questionList=ArrayList<Listening>()

        val que1= Listening(
            1,
            "I can follow the gist of everyday conversation when people speak clearly to me in standard dialect",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que1)

        val que2= Listening(
            2,
            "I can understand straightforward factual information about everyday, study- or work-related topics, identifying both general messages and specific details, provided speech is clearly articulated in a generally familiar accent.",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que2)

        val que3= Listening(
            3,
            "I can understand the main points of discussions on familiar topics in everyday situations when people speak clearly in standard dialect",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que3)

        val que4= Listening(
            4,
            "I can follow a lecture or talk within my own academic or professional field, provided the subject matter is familiar and the presentation straightforward and clearly structured",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que4)

        val que5= Listening(
            5,
            "I can catch the main elements of radio news bulletins and recorded audio material on familiar topics delivered in clear standard speech",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que5)

        val que6= Listening(
            6,
            " I can follow many TV programmes on topics of personal or cultural interest broadcast in standard dialect",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que6)

        val que7= Listening(
            7,
            "I can follow many films in which visuals and action carry much of the storyline, when the language is clear and straightforward",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que7)

        val que8= Listening(
            8,
            "I can follow detailed directions, messages and information (e.g., travel arrangements, recorded weather forecasts, answering-machines)",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que8)

        val que9= Listening(
            9,
            "I can understand simple technical information, such as operating instructions for everyday equipment",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que9)

        val que10= Listening(
            10,
            "I can understand a short narrative well enough to be able to guess what may happen next",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que10)
// Second step
        val que11= Listening(
            11,
            "I can understand standard spoken language on both familiar and unfamiliar topics in everyday situations even in a noisy environment",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que11)
        val que12= Listening(
            12,
            "I can with some effort catch much of what is said around me, but may find it difficult to understand a discussion between several native speakers who do not modify their language in any way",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que12)

        val que13= Listening(
            13,
            "I can understand announcements and messages on concrete and abstract topics spoken in standard dialect at normal speed",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que13)

        val que14= Listening(
            14,
            "I can follow extended talks delivered in standard dialect on cultural, intercultural and social issues (e.g., customs, media, lifestyle, EU)",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que14)

        val que15= Listening(
            15,
            "I can follow complex lines of argument, provided these are clearly signposted and the topic is reasonably familiar",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que15)

        val que16= Listening(
            16,
            "I can follow the essentials of lectures, talks and reports and other forms of academic or professional presentation in my field",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que16)
        val que17= Listening(
            17,
            " I can follow most TV news programmes, documentaries, interviews, talk shows and the majority of films in standard dialect",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que17)


        val que18= Listening(
            18,
            "I can follow most radio programmes and audio material delivered in standard dialect and identify the speaker’s mood, tone, etc.",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que18)

        val que19= Listening(
            19,
            "I am sensitive to expressions of feeling and attitudes (e.g., critical, ironic, supportive, flippant, disapproving)",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que19)

        // finished the second ones.
        val que20= Listening(
            20,
            "I can follow extended speech even when it is not clearly structured and when relationships are only implied and not signalled explicitly ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que20)
        val que21= Listening(
            21,
            "I can recognise a wide range of idiomatic expressions and colloquialisms, appreciating register shifts",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que21)
        val que22= Listening(
            22,
            "I can understand enough to follow extended speech on abstract and complex topics of academic or vocational relevance, though I may need to confirm occasional details, especially if the accent is unfamiliar",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que22)
        val que23= Listening(
            23,
            "I can easily follow complex interactions between third parties in group discussion and debate, even on abstract and unfamiliar topics",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que23)
        val que24= Listening(
            24,
            "I can follow most lectures, discussions and debates in my academic or professional field with relative ease",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que24)
        val que25= Listening(
            25,
            "I can understand complex technical information, such as operating instructions and specifications for familiar products and services",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que25)
        val que26= Listening(
            26,
            "I can extract specific information from poor quality, audibly distorted public announcements (e.g., in a station, sports stadium, etc.)",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )

        questionList.add(que26)

        val que27= Listening(
            27,
            "I can understand a wide range of recorded and broadcast audio material, including some non-standard usage, and identify finer points of detail including implicit attitudes and relationships between speakers",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que27)

        val que28= Listening(
            28,
            "I can follow films employing a considerable degree of slang and idiomatic usage",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que28)

        return questionList
    }

}