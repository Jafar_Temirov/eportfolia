package com.jafar.portfolia.util.myachievement

import com.jafar.portfolia.model.Listening
import com.jafar.portfolia.model.Reading
import com.jafar.portfolia.model.Spoken_production

object SpokenProductionQuestion{

    fun getSpokenProductionQuiz() :ArrayList<Spoken_production>{
        val questionList=ArrayList<Spoken_production>()

        val que1= Spoken_production(
            1,
            "I can give a straightforward description of a subject within my academic or professional field, presenting it as a linear sequence of points",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que1)

        val que2= Spoken_production(
            2,
            "I can narrate a story or relate the plot of a film or book",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que2)

        val que3= Spoken_production(
            3,
            "I can describe personal experiences, reactions, dreams, hopes, ambitions, real, imagined or unexpected events",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que3)

        val que4= Spoken_production(
            4,
            "I can briefly give reasons and explanations for opinions, plans and actions",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que4)

        val que5= Spoken_production(
            5,
            "I can develop an argument well enough to be followed without difficulty most of the time",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que5)

        val que6= Spoken_production(
            6,
            "I can give a simple summary of short written texts",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que6)

        val que7= Spoken_production(
            7,
            "I can give detailed accounts of problems and incidents (e.g., reporting a theft, traffic accident)",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que7)

        val que8= Spoken_production(
            8,
            "I can deliver short rehearsed announcements and statements on everyday matters within my field",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que8)

        val que9= Spoken_production(
            9,
            "I can give a short and straightforward prepared presentation on a chosen topic in my academic or professional field in a reasonably clear and precise manner",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que9)

        val que10= Spoken_production(
            10,
            "I can explain simply how to use a piece of equipment",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que10)
// Second step
        val que11= Spoken_production(
            11,
            "I can give clear detailed descriptions on a wide range of subjects relating to my field, expanding and supporting ideas with subsidiary points and relevant examples",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que11)
        val que12= Spoken_production(
            12,
            "I can explain a viewpoint on a topical issue, giving the advantages and disadvantages of various options",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que12)

        val que13= Spoken_production(
            13,
            "I can develop a clear coherent argument, linking ideas logically and expanding and supporting my points with appropriate examples",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que13)

        val que14= Spoken_production(
            14,
            "I can outline an issue or a problem clearly, speculating about causes, consequences and hypothetical situations",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que14)

        val que15= Spoken_production(
            15,
            "I can summarise short discursive or narrative material (e.g., written text, radio, television)",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que15)

        val que16= Spoken_production(
            16,
            "I can deliver announcements on most general topics with a degree of clarity, fluency and spontaneity which causes no strain or inconvenience to the listener",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que16)
        val que17= Spoken_production(
            17,
            "I can give a clear, systematically developed presentation on a topic in my field, with highlighting of significant points and relevant supporting detail",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que17)

        val que18= Spoken_production(
            18,
            "I can depart spontaneously from a prepared text and follow up points raised by an audience",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que18)
        // finished the second ones.
        val que19= Spoken_production(
            19,
            "I can give clear detailed summaries of complex subjects in my field",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que19)


        val que20= Spoken_production(
            20,
            "I can develop a detailed argument or narrative, integrating sub-themes, developing particular points and rounding off with an appropriate conclusion",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que20)
        val que21= Spoken_production(
            21,
            "I can give a clear, well-structured presentation on a complex subject in my field, expanding and supporting points of view with appropriate reasons and examples  ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que21)
        val que22= Spoken_production(
            22,
            "I can put together information from different sources and relate it in a coherent summary ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que22)
        val que23= Spoken_production(
            23,
            "I can present oral summaries of long, demanding texts",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que23)
        val que24= Spoken_production(
            24,
            "I can deliver announcements fluently, almost effortlessly, using stress and intonation to convey finer shades of meaning precisely",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que24)

        return questionList
    }

}