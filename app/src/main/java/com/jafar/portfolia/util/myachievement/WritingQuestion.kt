package com.jafar.portfolia.util.myachievement

import com.jafar.portfolia.model.Listening
import com.jafar.portfolia.model.Reading
import com.jafar.portfolia.model.Writing2

object WritingQuestion{

    fun getWritingQuiz() :ArrayList<Writing2>{
        val questionList=ArrayList<Writing2>()

        val que1= Writing2(
            1,
            "I can write a description of an event (e.g., a recent trip), real or imagined",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que1)

        val que2= Writing2(
            2,
            "I can write notes conveying simple information of immediate relevance to people who feature in my everyday life, getting across comprehensibly the points I feel are important",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que2)

        val que3= Writing2(
            3,
            "I can write personal letters giving news, describing experiences and impressions, and expressing feelings",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que3)

        val que4= Writing2(
            4,
            "I can take down messages communicating enquiries and factual information, explaining problems",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que4)

        val que5= Writing2(
            5,
            "I can write straightforward connected texts and simple essays on familiar subjects within my field, by linking a series of shorter discrete elements into a linear sequence, and using dictionaries and reference resources as necessary",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que5)

        val que6= Writing2(
            6,
            "I can summarise the plot of a film or book, or narrate a simple story",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que6)

        val que7= Writing2(
            7,
            "I can write very brief reports to a standard conventionalised format, which pass on routine factual information on matters relating to my field",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que7)

        val que8= Writing2(
            8,
            "I can write formal letters giving or requesting detailed information (e.g., replying to an advertisement, applying for a job)",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que8)

        val que9= Writing2(
            9,
            "I can write simple, clear instructions about work routines or how a machine works",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que9)
// Second step
        val que10= Writing2(
            10,
            "I can write clear detailed text on a wide range of subjects relating to my personal, academic or professional interests",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que10)

        val que11= Writing2(
            11,
            "I can write letters conveying degrees of emotion and highlighting the personal significance of events and experiences, and commenting on the correspondent’s news and views",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que11)
        val que12= Writing2(
            12,
            "I can express news, views and feelings effectively in writing, and relate to the views and feelings of others",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que12)

        val que13= Writing2(
            13,
            "I can write summaries of articles on topics of general, academic or professional interest, extracting information from different sources and media",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que13)

        val que14= Writing2(
            14,
            "I can write an essay or report which develops an argument, giving reasons to support or negate a point of view, weighing pros and cons",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que14)

        val que15= Writing2(
            15,
            " I can write clear detailed descriptions of real or imaginary events and experiences in a detailed and easily readable way, marking the relationship between ideas",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que15)

        val que16= Writing2(
            16,
            "I can write a short review of a film or book",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que16)
        val que17= Writing2(
            17,
            " I can write standard formal letters requesting or communicating relevant information, with appropriate use of register and conventions",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que17)
// third section

        val que18= Writing2(
            18,
            "I can express myself fluently and accurately in writing on a wide range of personal, academic or professional topics, varying my vocabulary and style according to the context",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que18)

        val que19= Writing2(
            19,
            "I can write clear, well-structured texts on complex subjects in my field, underlining the relevant salient issues, expanding and supporting points of view at some length with subsidiary points, reasons and relevant examples, and rounding off with an appropriate conclusion ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que19)

        val que20= Writing2(
            20,
            "I can write clear, detailed, well-structured and developed descriptions and imaginative texts in an assured, personal, natural style appropriate to the reader in mind ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que20)
        val que21= Writing2(
            21,
            "I can express myself with clarity and precision in personal correspondence, using language flexibly and effectively, including emotional, allusive and joking usage",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que21)
        val que22= Writing2(
            22,
            "I can elaborate my case effectively and accurately in complex formal letters (e.g., registering a complaint, taking a stand against an issue)  ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que22)

        return questionList
    }
}