package com.jafar.portfolia.util.myachievement

import com.jafar.portfolia.database.Entity.SpokenInterQ
import com.jafar.portfolia.model.Listening
import com.jafar.portfolia.model.Reading
import com.jafar.portfolia.model.Spoken_Interaction

object SpokenInteractionQuestion{

    fun getSpokenInterQuiz() :ArrayList<Spoken_Interaction>{
        val questionList=ArrayList<Spoken_Interaction>()

        val que1= Spoken_Interaction(
            1,
            "I can exchange, check and confirm factual information on familiar routine and non-routine matters within my field with some confidence",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que1)

        val que2= Spoken_Interaction(
            2,
            "I can express and respond to feelings and attitudes (e.g., surprise, happiness, sadness, interest, uncertainty, indifference)",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que2)

        val que3= Spoken_Interaction(
            3,
            "I can express my thoughts about abstract or cultural topics such as music or films, and give brief comments on the views of others",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que3)

        val que4= Spoken_Interaction(
            4,
            "I can explain why something is a problem, discuss what to do next, compare and contrast alternatives",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que4)

        val que5= Spoken_Interaction(
            5,
            "I can obtain detailed information and can ask for and follow detailed directions",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que5)

        val que6= Spoken_Interaction(
            6,
            "I can handle most practical tasks in everyday situations (e.g., making telephone enquiries, asking for a refund, negotiating purchase)",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que6)

        val que7= Spoken_Interaction(
            7,
            "I can provide concrete information required in an interview/consultation (e.g., describe symptoms to a doctor), but with limited precision",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que7)

        val que8= Spoken_Interaction(
            8,
            "I can take some initiatives in an interview/consultation (e.g., bring up a new subject) but am very dependent on the interviewer to provide support",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que8)

        val que9= Spoken_Interaction(
            9,
            "I can use a prepared questionnaire to carry out a structured interview, with some spontaneous follow-up questions",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que9)
// Second step
        val que10= Spoken_Interaction(
            10,
            "I can participate fully in conversations on general topics with a degree of fluency and naturalness, and appropriate use of register",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que10)

        val que11= Spoken_Interaction(
            11,
            "I can participate effectively in extended discussions and debates on subjects of personal, academic or professional interest, marking clearly the relationship between ideas",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que11)
        val que12= Spoken_Interaction(
            12,
            "I can account for and sustain my opinion in discussion by providing relevant explanations, arguments and comments",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que12)

        val que13= Spoken_Interaction(
            13,
            "I can express, negotiate and respond sensitively to feelings, attitudes, opinions, tone, viewpoints",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que13)

        val que14= Spoken_Interaction(
            14,
            "I can exchange detailed factual information on matters within my academic or professional field ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que14)

        val que15= Spoken_Interaction(
            15,
            "I can help along the progress of a project by inviting others to join in, express their opinions, etc.",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que15)

        val que16= Spoken_Interaction(
            16,
            "I can cope linguistically with potentially complex problems in routine situations (e.g., complaining about goods and services ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que16)
        val que17= Spoken_Interaction(
            17,
            "I can cope adequately with emergencies (e.g., summon medical assistance, telephone the police or breakdown service) ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que17)


        val que18= Spoken_Interaction(
            18,
            "I can handle personal interviews with ease, taking initiatives and expanding ideas with little help or prodding from an interviewer",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que18)

        val que19= Spoken_Interaction(
            19,
            "I can carry out an effective, fluent interview, departing spontaneously from prepared questions, following up and probing interesting replies ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que19)

        // finished the second ones.
        val que20= Spoken_Interaction(
            20,
            "I can use language flexibly and effectively for social purposes, including emotional, allusive and joking usage ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que20)
        val que21= Spoken_Interaction(
            21,
            "I can participate effectively in extended debates on abstract and complex topics of a specialist nature in my academic or professional field",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que21)
        val que22= Spoken_Interaction(
            22,
            "I can easily follow and contribute to complex interactions between third parties in group discussion even on abstract or less familiar topics ",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que22)
        val que23= Spoken_Interaction(
            23,
            "I can argue a formal position convincingly, responding to questions and comments and answering complex lines of counter argument fluently, spontaneously and appropriately",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que23)
        val que24= Spoken_Interaction(
            24,
            "I can participate fully in an interview, either as interviewer or as interviewee, fluently expanding and developing the point under discussion, and handling interjections with confidence",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que24)
        val que25= Spoken_Interaction(
            25,
            "I can use the telephone for a variety of purposes, including solving problems and misunderstanding, though I may need to ask for clarification if the accent is unfamiliar",
            "I can do it easily",
            "I can do it with help",
            "Its my objective",
            1
        )
        questionList.add(que25)

        return questionList
    }

}