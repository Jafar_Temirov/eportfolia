package com.jafar.portfolia.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.widget.Toast

class Function {
      companion object{
          fun showToast(context: Context, message: String){
              Toast.makeText(context,message, Toast.LENGTH_SHORT).show()
          }

          fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
              val matrix = Matrix()
              matrix.postRotate(angle)
              return Bitmap.createBitmap(
                  source, 0, 0, source.width, source.height,
                  matrix, true
              )
          }
      }
}