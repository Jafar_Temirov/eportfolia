package com.jafar.portfolia.ui.languagePassport

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jafar.portfolia.R
import com.jafar.portfolia.adapter.Passport.HigherEducationAdapter
import com.jafar.portfolia.adapter.Passport.PrimaryEducationAdapter
import com.jafar.portfolia.adapter.Passport.SecondaryEducationAdapter
import com.jafar.portfolia.database.MainDatabase
import com.jafar.portfolia.database.PassportEntity.Primary_edu
import com.jafar.portfolia.database.PassportEntity.higher_edu
import com.jafar.portfolia.database.PassportEntity.Secondary_edu
import com.jafar.portfolia.repository.PassportRepository
import com.jafar.portfolia.util.Function
import com.jafar.portfolia.viewmodel.PassportViewModel
import com.jafar.restaurants.ui.viewmodel.PassportViewModelFactory
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.bottom_sheet_persistent.*
import kotlinx.android.synthetic.main.fragment_summaryof_language.*
import java.text.SimpleDateFormat
import java.util.*


class SummaryofLanguageFragment : Fragment() {
    lateinit var viewModel: PassportViewModel
    var sheetBehavior: BottomSheetBehavior<*>? = null
    var integer=0
    private val calendar = Calendar.getInstance()
    // Recyclerview1
    lateinit var adapter: PrimaryEducationAdapter
    lateinit var recyclerview: RecyclerView
    private var layoutManager: RecyclerView.LayoutManager? = null
    //Recyclerview2
    lateinit var adapter2: SecondaryEducationAdapter
    lateinit var recyclerview2: RecyclerView
    private var layoutManager2: RecyclerView.LayoutManager? = null
    // Recyclerview3
    lateinit var adapter3: HigherEducationAdapter
    lateinit var recyclerview3: RecyclerView
    private var layoutManager3: RecyclerView.LayoutManager? = null
    // for size
    var primary=0
    var secondary=0
    var higher=0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?):View? {
        return inflater.inflate(R.layout.fragment_summaryof_language, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val repository= PassportRepository(MainDatabase(requireActivity()))
        val viewModelProviderFactory = PassportViewModelFactory(requireActivity().application, repository)
        viewModel = ViewModelProvider(this, viewModelProviderFactory).get(PassportViewModel::class.java)
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        back_summary.setOnClickListener {this.findNavController().popBackStack()}
        recyclerview=view.findViewById(R.id.primary_recyclerview)
        recyclerview2=view.findViewById(R.id.secondary_recyclerview)
        recyclerview3=view.findViewById(R.id.higher_recyclerview)
        layoutManager= LinearLayoutManager(activity)
        layoutManager2= LinearLayoutManager(activity)
        layoutManager3= LinearLayoutManager(activity)
        recyclerview.layoutManager = layoutManager
        recyclerview2.layoutManager=layoutManager2
        recyclerview3.layoutManager=layoutManager3
        recyclerview.apply {
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }
        recyclerview2.apply {
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }
        recyclerview3.apply {
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }

        viewModel.getPrimary_edu().observe(viewLifecycleOwner, Observer {list->
            adapter= PrimaryEducationAdapter(requireActivity(),list)
            recyclerview.adapter=adapter
            adapter.notifyDataSetChanged()
            primary=list.size
        })
        viewModel.getSecondary_edu().observe(viewLifecycleOwner, Observer {list->
            adapter2= SecondaryEducationAdapter(requireActivity(),list)
            recyclerview2.adapter=adapter2
            adapter2.notifyDataSetChanged()
            secondary=list.size
        })
        viewModel.getHigher_edu().observe(viewLifecycleOwner, Observer {
            adapter3= HigherEducationAdapter(requireActivity(),it)
            recyclerview3.adapter=adapter3
            adapter3.notifyDataSetChanged()
            higher=it.size
        })

        add_primary.setOnClickListener {
            if (primary<=3){
                BottomSheet()
                integer=1
            } else Function.showToast(requireActivity(),"The number of language should not be more than 4")

        }
        btn_secondary.setOnClickListener {
            if (secondary<=3) {
                BottomSheet()
                integer = 2
            }else Function.showToast(requireActivity(),"The number of language should not be more than 4")
        }
        btn_higher.setOnClickListener {
            if (higher<=3){
            BottomSheet()
            integer=3
            }else Function.showToast(requireActivity(),"The number of language should not be more than 4")
        }

        save_bottomsheet.setOnClickListener {
             saveBottomsheet(integer)
        }

        date_of_birth_bottomsheet.setOnClickListener {
            DatePickerDialog(
                requireActivity(), date, calendar[Calendar.YEAR],
                calendar[Calendar.MONTH],
                calendar[Calendar.DAY_OF_MONTH]
            ).show()
        }

        date_started_bottomsheet.setOnClickListener {
            DatePickerDialog(
                requireActivity(), date2, calendar[Calendar.YEAR],
                calendar[Calendar.MONTH],
                calendar[Calendar.DAY_OF_MONTH]
            ).show()
        }
    }

    var date =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar[Calendar.YEAR] = year
            calendar[Calendar.MONTH] = monthOfYear
            calendar[Calendar.DAY_OF_MONTH] = dayOfMonth
            setDateOfBirth()
        }

    var date2 =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar[Calendar.YEAR] = year
            calendar[Calendar.MONTH] = monthOfYear
            calendar[Calendar.DAY_OF_MONTH] = dayOfMonth
            setDateStartUsing()
        }

    private fun setDateOfBirth() {
        val myFormat = "dd MMMM yyyy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        date_of_birth_bottomsheet?.setText(sdf.format(calendar.getTime()))
    }

    private fun setDateStartUsing() {
        val myFormat = "dd MMMM yyyy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        date_started_bottomsheet?.setText(sdf.format(calendar.getTime()))
    }

    private fun saveBottomsheet(id:Int){
 if (language_bottomsheet.text.toString().isNotEmpty()&&
         details_bottomsheet.text.toString().isNotEmpty()&&
         date_of_birth_bottomsheet.text.toString().isNotEmpty()&&
         date_started_bottomsheet.text.toString().isNotEmpty())  {

     val primaryprimary_edu=Primary_edu(
         language_bottomsheet.text.toString(),
         details_bottomsheet.text.toString(),
         date_of_birth_bottomsheet.text.toString(),
         date_started_bottomsheet.text.toString())
     val secondary_edu=Secondary_edu(
         language_bottomsheet.text.toString(),
         details_bottomsheet.text.toString(),
         date_of_birth_bottomsheet.text.toString(),
         date_started_bottomsheet.text.toString())
     val higherEdu=higher_edu(
         language_bottomsheet.text.toString(),
         details_bottomsheet.text.toString(),
         date_of_birth_bottomsheet.text.toString(),
         date_started_bottomsheet.text.toString()
     )

     when(id){
         1-> {
             Function.showToast(requireActivity(),"Data saved")
             viewModel.savePrimary_edu(primaryprimary_edu)
             BottomSheet()
         }
         2-> {
             Function.showToast(requireActivity(),"Data saved")
             viewModel.saveSecondary_edu(secondary_edu)
             BottomSheet()
         }
         3-> {
             Function.showToast(requireActivity(),"Data saved")
             viewModel.saveHigher_edu(higherEdu)
             BottomSheet()
         }
         else-> {Function.showToast(requireActivity(),"There is no data")}
     }
      }
 else Function.showToast(requireActivity(),"All filed should be filled")
    }

    private fun BottomSheet(){
        if ((sheetBehavior as BottomSheetBehavior<*>).state==BottomSheetBehavior.STATE_EXPANDED){
            (sheetBehavior as BottomSheetBehavior<*>).state=BottomSheetBehavior.STATE_COLLAPSED
        }else if ((sheetBehavior as BottomSheetBehavior<*>).state==BottomSheetBehavior.STATE_COLLAPSED){
            (sheetBehavior as BottomSheetBehavior<*>).state=BottomSheetBehavior.STATE_EXPANDED
        }
        language_bottomsheet.setText("")
        details_bottomsheet.setText("")
        date_of_birth_bottomsheet.setText("")
        date_started_bottomsheet.setText("")
    }

}