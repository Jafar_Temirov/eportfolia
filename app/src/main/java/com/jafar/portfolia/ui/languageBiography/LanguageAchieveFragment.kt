package com.jafar.portfolia.ui.languageBiography

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.jafar.portfolia.R
import kotlinx.android.synthetic.main.fragment_language_achieve.*

class LanguageAchieveFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_language_achieve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cardview_listening.setOnClickListener {
            this.findNavController().navigate(R.id.action_languageAchieveFragment_to_listeningQuizFragment)
        }
        cardview_reading.setOnClickListener {
            this.findNavController().navigate(R.id.action_languageAchieveFragment_to_readingQuizFragment)
        }
        cardview_spoken_interaction.setOnClickListener {
            this.findNavController().navigate(R.id.action_languageAchieveFragment_to_spokenInteractionFragment)

        }
        cardview_spoken_production.setOnClickListener {
            this.findNavController().navigate(R.id.action_languageAchieveFragment_to_spokenProductionFragment)

        }

        cardview_writing.setOnClickListener {
            this.findNavController().navigate(R.id.action_languageAchieveFragment_to_writingQuizFragment)

        }
        back_self_assessment.setOnClickListener {
            this.findNavController().popBackStack()
        }

    }

    private fun  openIntent(uri: Uri){
        val intent = Intent()
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.action = Intent.ACTION_VIEW
        val type = "application/msword"
        intent.setDataAndType(uri, type)
        startActivity(Intent.createChooser(intent,"Open a word documentation"))
    }

    /*val file: File =File(path.path)
           val uri = FileProvider.getUriForFile(
               requireActivity(),
               BuildConfig.APPLICATION_ID + ".provider",
               file
           )*/
    //Uri uri = Uri.parse("file://"+file.getAbsolutePath());

}