package com.jafar.portfolia.ui.dossier

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.FileUriExposedException
import android.os.strictmode.FileUriExposedViolation
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jafar.portfolia.R
import com.jafar.portfolia.adapter.WritingAdapter
import com.jafar.portfolia.database.Entity.Writing
import com.jafar.portfolia.ui.activity.MainActivity
import com.jafar.portfolia.util.Function.Companion.showToast
import com.jafar.restaurants.ui.viewmodel.RegisterViewModel
import kotlinx.android.synthetic.main.fragment_writing.*
import java.lang.IllegalStateException


class WritingFragment : Fragment(),WritingAdapter.OnItemClickListener {

    lateinit var adapter: WritingAdapter
    lateinit var recyclerview: RecyclerView
    private var layoutManager: RecyclerView.LayoutManager? = null
    var viewModel: RegisterViewModel?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?{return inflater.inflate(R.layout.fragment_writing, container, false) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel= (activity as MainActivity).viewModel
        recyclerview=view.findViewById(R.id.recyclerview_writing)
        layoutManager= LinearLayoutManager(activity)
        recyclerview.layoutManager = layoutManager
        recyclerview.apply {
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }

        viewModel!!.getWriting().observe(viewLifecycleOwner, Observer {
            adapter = WritingAdapter(requireActivity(), it, this)
            recyclerview.adapter = adapter
            adapter.notifyDataSetChanged()
            Log.e("TAG", it.toString() + "Resposne")
        })
        view.findViewById<View>(R.id.float_writing).setOnClickListener {
            this.findNavController().navigate(R.id.action_writingFragment_to_writingAddFragment)
        }
        back_all_writing.setOnClickListener {
            this.findNavController().popBackStack()
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onItemClick(item: Writing, position: Int) {
        showToast(requireActivity(), "Position: ${item.file_uri}")
        try {
            val uri=(item.file_uri).replace("file","file")
            val browserIntent = Intent(Intent.ACTION_VIEW)
            browserIntent.setDataAndType(Uri.parse(uri), "application/pdf")
            val chooser = Intent.createChooser(browserIntent, "Pdf ochadigan dasturni tanlang !!!")
            chooser.flags = Intent.FLAG_ACTIVITY_NEW_TASK // optional
            startActivity(chooser)
        }catch (e: FileUriExposedException){
            val uri=(item.file_uri).replace("file","content")
            val browserIntent = Intent(Intent.ACTION_VIEW)
            browserIntent.setDataAndType(Uri.parse(uri), "application/pdf")
            val chooser = Intent.createChooser(browserIntent, "Pdf ochadigan dasturni tanlang !!!")
            chooser.flags = Intent.FLAG_ACTIVITY_NEW_TASK // optional
            startActivity(chooser)
        }

    }

}