package com.jafar.portfolia.ui.dialogfragment

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.github.barteksc.pdfviewer.PDFView
import com.itextpdf.text.*
import com.itextpdf.text.pdf.*
import com.jafar.portfolia.BuildConfig
import com.jafar.portfolia.R
import com.jafar.portfolia.database.Entity.MyAims
import com.jafar.portfolia.database.Entity.MyDiary
import com.jafar.portfolia.database.MainDatabase
import com.jafar.portfolia.database.PassportEntity.*
import com.jafar.portfolia.repository.PassportRepository
import com.jafar.portfolia.repository.RegisterRepository
import com.jafar.portfolia.util.Function
import com.jafar.portfolia.util.LoadingDialog
import com.jafar.portfolia.viewmodel.PassportViewModel
import com.jafar.restaurants.ui.viewmodel.PassportViewModelFactory
import com.jafar.restaurants.ui.viewmodel.RegisterViewModel
import com.jafar.restaurants.ui.viewmodel.RegisterViewModelFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.*

class PdfPortfoliaFragment : DialogFragment(){
    lateinit var viewModel: RegisterViewModel
    lateinit var viewModel2: PassportViewModel
    lateinit var loadingDialog: LoadingDialog
    val real_list:MutableList<File> = ArrayList()
    val TAG="PdfPortfoliaFragment"
    var pdfView:PDFView?=null
    var uri:Uri?=null
    // writin certificate
    var writing=""
    var strings=""
    var strings2=""
    var strings3=""
    // this is for viewmodel
    var address=""
    // fixed
    var name=""
    var date_of_birth=""
    var place_of_birth=""
    var nationality=""
    var mother_tonuge=""
    var date_using=""
    // second
    var lis=""
    var read=""
    var write=""
    var spok_p=""
    var spok_i=""
     // other sides
     var myAims:List<MyAims>?=null
     var myDiary:List<MyDiary>?=null
     // my other sides
     var primary:List<Primary_edu>?=null
     var secondary:List<Secondary_edu>?=null
     var higherEdu:List<higher_edu>?=null
     // Passport
     var languageWork:List<Language_work>?=null
     var linguisticStudy:List<Linguistic_study>?=null
     var travellingLiving:List<Travelling_living>?=null

    private val SPLASH_SCREEN =5000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullscreenDialogtheme)}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View= inflater.inflate(R.layout.fragment_pdf_portfolia, container, false)
        val repository= RegisterRepository(MainDatabase(requireActivity()))
        val repository2=PassportRepository(MainDatabase(requireActivity()))
        val viewModelProviderFactory = RegisterViewModelFactory(
            requireActivity().application,
            repository
        )
        val viewModelProviderFactory2=PassportViewModelFactory(
            requireActivity().application,
            repository2
        )
        viewModel = ViewModelProvider(this, viewModelProviderFactory).get(RegisterViewModel::class.java)
        viewModel2=ViewModelProvider(this, viewModelProviderFactory2).get(PassportViewModel::class.java)
        loadingDialog= LoadingDialog(requireActivity())

        pdfView=view.findViewById(R.id.pdfview_portfolia)
        view.findViewById<View>(R.id.myown_exit5).setOnClickListener {dismiss()}
        view.findViewById<View>(R.id.share_pdf).setOnClickListener {
            if (uri!=null){
                val shareIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_STREAM, uri)
                    type = "image/*"
                }
                startActivity(
                    Intent.createChooser(
                        shareIntent,
                        resources.getText(R.string.app_name)
                    )
                )
            }else Function.showToast(
                requireActivity(),
                "Before Sharing the file , Please Click Generate Button"
            )

        }

        view.findViewById<View>(R.id.btn_pdf_save).setOnClickListener {
            try {
                loadingDialog.startLoading()
                GlobalScope.launch { createPdfWrapper() }

                Handler().postDelayed({
                    loadingDialog.dismissDialog()
                }, SPLASH_SCREEN.toLong())

            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: DocumentException) {
                e.printStackTrace()
            }
        }

        viewModel.getRegistration().observe(viewLifecycleOwner, Observer { response ->
            for (i in response) {
                address = i.image_uri
                name = i.name
                date_of_birth = i.date_of_birth
                place_of_birth = i.place_of_birth
                nationality = i.nationality
                mother_tonuge = i.mother_tongue
                date_using = i.date_using
            }
        })
        viewModel.getSelf_Assessment().observe(viewLifecycleOwner, Observer { list ->
            for (i in list) {
                lis = i.listening
                read = i.reading
                write = i.writing
                spok_i = i.spoken_interaction
                spok_p = i.spoken_production
            }
        })
        viewModel.getAims().observe(viewLifecycleOwner, Observer {
            myAims = it
            Log.d("Response: ", myAims.toString())
        })
        viewModel.getDiary().observe(viewLifecycleOwner, Observer {
            myDiary = it
            Log.d("Diary Response: ", myDiary.toString())
        })

        viewModel.getCertificate().observe(viewLifecycleOwner, Observer {
            var ii = 0
            for (i in it) {
                Log.d(TAG, "Certificate $i " + i.file_uri)
                when (ii) {
                    0 -> strings = i.file_uri
                    1 -> strings2 = i.file_uri
                    else -> strings3 = i.file_uri
                }
                ii++
                real_list.add(File(Uri.parse(i.file_uri).path!!))
            }

        })

        viewModel.getWriting().observe(viewLifecycleOwner, Observer {
            for (i in it) {
                Log.d(TAG, "Writing $i " + i.file_uri)
                if (i.file_uri!=null&&i.file_uri.isNotEmpty())
                    real_list.add(File(Uri.parse(i.file_uri).path!!))
            }
        })

        viewModel2.getPrimary_edu().observe(viewLifecycleOwner, Observer {
            primary = it
        })
        viewModel2.getSecondary_edu().observe(viewLifecycleOwner, Observer {
            secondary = it
        })
        viewModel2.getHigher_edu().observe(viewLifecycleOwner, Observer {
            higherEdu = it
        })
        viewModel2.getLanguageWork().observe(viewLifecycleOwner, Observer {
            languageWork = it
        })
        viewModel2.getLinguisticStudy().observe(viewLifecycleOwner, Observer {
            linguisticStudy = it
        })
        viewModel2.getTravelkLiving().observe(viewLifecycleOwner, Observer {
            travellingLiving = it
        })

        return view
    }

    private suspend fun createPdfWrapper(){
        val file = File(
            Environment.getExternalStorageDirectory(),
            System.currentTimeMillis().toString() + ".pdf"
        )
        val output: OutputStream = FileOutputStream(file)
        val document = Document(PageSize.A4)
        // First page
        val table = PdfPTable(floatArrayOf(3f, 3f, 3f, 3f, 3f))
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER)
        table.getDefaultCell().setFixedHeight(50f)
        table.setTotalWidth(PageSize.A4.width)
        table.setWidthPercentage(100f)
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE)
        table.addCell("Listening")
        table.addCell("Reading")
        table.addCell("Writing")
        table.addCell("Spoken Interaction")
        table.addCell("Spoken Production")
        table.setHeaderRows(1)
        val cells: Array<PdfPCell> = table.getRow(0).getCells()
        for (j in cells.indices) {
            cells[j].setBackgroundColor(BaseColor(Color.parseColor("#dcddde")))
        }
        table.addCell("$lis");
        table.addCell("$read");
        table.addCell("$write");
        table.addCell("$spok_i");
        table.addCell("$spok_p");

        PdfWriter.getInstance(document, output)
        document.open()
        val f = Font(Font.FontFamily.TIMES_ROMAN, 28.0f, Font.NORMAL, BaseColor.BLACK)
        val g = Font(Font.FontFamily.TIMES_ROMAN, 24.0f, Font.NORMAL, BaseColor.BLACK)
        document.add(Paragraph("Uzbek Model of European Language Portfolio \n\n", f))
        document.add(Paragraph("Name: ${name} \n", g))
        document.add(Paragraph("Date of birth:  ${date_of_birth} \n", g))
        document.add(Paragraph("Place of birth:  ${place_of_birth} \n", g))
        document.add(Paragraph("Nationality:  ${nationality} \n", g))
        document.add(Paragraph("My native language:  ${mother_tonuge} \n", g))
        document.add(Paragraph("Date of starting using ELP:  ${date_using} \n\n", g))

        document.add(Paragraph("\n\n\n Self Assessment \n\n", g))
        document.add(Paragraph("My score: \n\n", g))
        document.add(table)
        // Second Page
        document.newPage()
        document.open()
        val table4 = PdfPTable(floatArrayOf(3f, 3f, 3f, 3f))
        table4.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER)
        table4.getDefaultCell().setFixedHeight(55f)
        table4.setTotalWidth(PageSize.A4.width)
        table4.setWidthPercentage(100f)
        table4.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE)
        table4.addCell("Language")
        table4.addCell("Details")
        table4.addCell("From")
        table4.addCell("To")

        table4.setHeaderRows(1)
        val cells4 : Array<PdfPCell> = table4.getRow(0).getCells()
        for (j in cells4.indices) {
            cells4[j].setBackgroundColor(BaseColor(Color.parseColor("#dcddde")))
        }
        for (i in primary!!){
            table4.addCell(i.language);
            table4.addCell(i.details)
            table4.addCell(i.from)
            table4.addCell(i.to)
        }

        val f222 = Font(Font.FontFamily.TIMES_ROMAN, 30.0f, Font.NORMAL, BaseColor.BLACK)
        document.add(Paragraph("\n" + "Primary Education \n\n", g))
        document.add(table4)

        val table5 = PdfPTable(floatArrayOf(3f, 3f, 3f, 3f))
        table5.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER)
        table5.getDefaultCell().setFixedHeight(40f)
        table5.setTotalWidth(PageSize.A4.width)
        table5.setWidthPercentage(100f)
        table5.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE)
        table5.addCell("Language")
        table5.addCell("Details")
        table5.addCell("From")
        table5.addCell("To")

        table5.setHeaderRows(1)
        val cells5 : Array<PdfPCell> = table5.getRow(0).getCells()
        for (j in cells5.indices) {
            cells5[j].setBackgroundColor(BaseColor(Color.parseColor("#dcddde")))
        }
        for (i in secondary!!){
            table5.addCell(i.language);
            table5.addCell(i.details)
            table5.addCell(i.from)
            table5.addCell(i.to)
        }
        document.add(Paragraph("\n" + "Secondary Education \n\n", g))
        document.add(table5)

        val table6 = PdfPTable(floatArrayOf(3f, 3f, 3f, 3f))
        table6.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER)
        table6.getDefaultCell().setFixedHeight(40f)
        table6.setTotalWidth(PageSize.A4.width)
        table6.setWidthPercentage(100f)
        table6.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE)
        table6.addCell("Language")
        table6.addCell("Details")
        table6.addCell("From")
        table6.addCell("To")

        table6.setHeaderRows(1)
        val cells6 : Array<PdfPCell> = table6.getRow(0).getCells()
        for (j in cells6.indices){cells6[j].setBackgroundColor(BaseColor(Color.parseColor("#dcddde")))}
        for (i in higherEdu!!){
            table6.addCell(i.language)
            table6.addCell(i.details)
            table6.addCell(i.from)
            table6.addCell(i.to)
        }
        val f22 = Font(Font.FontFamily.TIMES_ROMAN, 30.0f, Font.NORMAL, BaseColor.BLACK)
          document.add(Paragraph("\n" + "Higher Education \n\n", g))
          document.add(table6)


        document.newPage()
        document.open()

        val table7 = PdfPTable(floatArrayOf(3f, 3f, 3f, 3f))
        table7.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER)
        table7.getDefaultCell().setFixedHeight(40f)
        table7.setTotalWidth(PageSize.A4.width)
        table7.setWidthPercentage(100f)
        table7.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE)
        table7.addCell("Language")
        table7.addCell("Details")
        table7.addCell("From")
        table7.addCell("To")

        table7.setHeaderRows(1)
        val cells7 : Array<PdfPCell> = table7.getRow(0).getCells()
        for (j in cells7.indices){cells7[j].setBackgroundColor(BaseColor(Color.parseColor("#dcddde")))}
        for (i in linguisticStudy!!){
            table7.addCell(i.language)
            table7.addCell(i.details)
            table7.addCell(i.from)
            table7.addCell(i.to)
        }
        if (travellingLiving!!.size>0){
          document.add(Paragraph("\n" + "Using languages for study or training \n\n", g))
          document.add(table7)
        }

        val table8 = PdfPTable(floatArrayOf(3f, 3f, 3f, 3f))
        table8.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER)
        table8.getDefaultCell().setFixedHeight(55f)
        table8.setTotalWidth(PageSize.A4.width)
        table8.setWidthPercentage(100f)
        table8.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE)
        table8.addCell("Language")
        table8.addCell("Details")
        table8.addCell("From")
        table8.addCell("To")

        table8.setHeaderRows(1)
        val cells8 : Array<PdfPCell> = table8.getRow(0).getCells()
        for (j in cells8.indices){cells7[j].setBackgroundColor(BaseColor(Color.parseColor("#dcddde")))}
        for (i in languageWork!!){
            table8.addCell(i.language)
            table8.addCell(i.details)
            table8.addCell(i.from)
            table8.addCell(i.to)
        }
        if (languageWork!!.size>0){
            document.add(Paragraph("\n" + "Using languages at work \n\n", g))
            document.add(table8)
        }

        val table9 = PdfPTable(floatArrayOf(3f, 3f, 3f, 3f))
        table9.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER)
        table9.getDefaultCell().setFixedHeight(55f)
        table9.setTotalWidth(PageSize.A4.width)
        table9.setWidthPercentage(100f)
        table9.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE)
        table9.addCell("Language")
        table9.addCell("Details")
        table9.addCell("From")
        table9.addCell("To")

        table9.headerRows = 1
        val cells9 : Array<PdfPCell> = table9.getRow(0).getCells()
        for (j in cells9.indices){cells7[j].setBackgroundColor(BaseColor(Color.parseColor("#dcddde")))}
        for (i in languageWork!!){
            table9.addCell(i.language)
            table9.addCell(i.details)
            table9.addCell(i.from)
            table9.addCell(i.to)
        }
        if (travellingLiving!!.size>0){
            document.add(
                Paragraph(
                    "\n" + "Using languages while travelling or living abroad\n\n",
                    g
                )
            )
            document.add(table9)
        }
        document.newPage()
        document.open()

        val table2 = PdfPTable(floatArrayOf(3f, 3f, 3f, 3f, 3f))
        table2.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER)
        table2.getDefaultCell().setFixedHeight(55f)
        table2.setTotalWidth(PageSize.A4.width)
        table2.setWidthPercentage(100f)
        table2.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE)
        table2.addCell("Language")
        table2.addCell("Why do you want to learn this language")
        table2.addCell("Exact things I want to learn in this language")
        table2.addCell("Which level do I want to reach according to CEF levels")
        table2.addCell("My purpose of learning language")

        table2.setHeaderRows(1)
        val cells2: Array<PdfPCell> = table2.getRow(0).getCells()
        for (j in cells2.indices) {
            cells2[j].setBackgroundColor(BaseColor(Color.parseColor("#dcddde")))
        }
        for (i in myAims!!){
            table2.addCell(i.language);
            table2.addCell(i.reason_of_learning)
            table2.addCell(i.exact_thing)
            table2.addCell(i.cef_level)
            table2.addCell(i.purpose_language)
        }

        val f2 = Font(Font.FontFamily.TIMES_ROMAN, 30.0f, Font.NORMAL, BaseColor.BLACK)
        val table3 = PdfPTable(floatArrayOf(3f, 3f, 3f, 3f, 3f, 3f))
        table3.defaultCell.horizontalAlignment = Element.ALIGN_CENTER
        table3.defaultCell.fixedHeight = 60f
        table3.setTotalWidth(PageSize.A4.width)
        table3.setWidthPercentage(100f)
        table3.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE)

        table3.addCell("How many hours did I speak on self-study this week")
        table3.addCell("what aspects of language or skills did I work on most")
        table3.addCell("When did I work productively")
        table3.addCell("Where did I work most productively")
        table3.addCell("What methods or ways did I use during the self-study")
        table3.addCell("What was outcome of my self-study")

        table3.setHeaderRows(1)
        val cells3: Array<PdfPCell> = table3.getRow(0).getCells()
        for (j in cells3.indices){
            cells3[j].backgroundColor = BaseColor(Color.parseColor("#dcddde"))
        }
        for (i in myDiary!!){
            table3.addCell(i.amount_of_time);
            table3.addCell(i.aspect_of_studying)
            table3.addCell(i.when_product)
            table3.addCell(i.where_product)
            table3.addCell(i.method_of_study)
            table3.addCell(i.outcome)
        }

        if (myDiary!!.size>0){
            document.add(Paragraph("My language aims \n\n", g))
            document.add(table2)
            document.add(Paragraph("\n" + "\nMy language learning Diary \n\n", g))
            document.add(table3)
        }

        document.close()
        val uris: Uri = FileProvider.getUriForFile(
            requireActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", file
        )
        //uri=uris
        try {
            Log.d(TAG, strings + strings2 + strings3)
            val fileee = File(
                Environment.getExternalStorageDirectory(),
                System.currentTimeMillis().toString() + "_MERGED.pdf"
            )
            real_list.add(0,file)
            concatenatePdfs(real_list,fileee)
            val urisss: Uri = FileProvider.getUriForFile(
                requireActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fileee
            )
            showPdfFromUri(urisss)
            uri=urisss
        }catch (e: java.lang.Exception){
            e.printStackTrace()
            Function.showToast(requireActivity(),e.localizedMessage)
        }
    }

    @Throws(DocumentException::class, IOException::class)
    fun concatenatePdfs(listOfPdfFiles: MutableList<File>, outputFile: File?) {
        val document = Document()
        val outputStream = FileOutputStream(outputFile)
        val copy: PdfCopy = PdfSmartCopy(document, outputStream)
        document.open()
        for (inFile in listOfPdfFiles) {
            val reader = PdfReader(inFile.absolutePath)
            copy.addDocument(reader)
            reader.close()
        }
        document.close()
    }

    private fun showPdfFromUri(uri: Uri?) {
        Log.d(TAG, uri.toString())
        pdfView!!.fromUri(uri)
            .defaultPage(0)
            .spacing(10)
            .load()
    }
    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        dialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
    }
}