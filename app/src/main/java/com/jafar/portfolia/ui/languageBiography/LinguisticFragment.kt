package com.jafar.portfolia.ui.languageBiography

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jafar.portfolia.R
import com.jafar.portfolia.adapter.Passport.LinguisticIntercultural.LanguageWorkAdapter
import com.jafar.portfolia.adapter.Passport.LinguisticIntercultural.StudyTrainingAdapter
import com.jafar.portfolia.adapter.Passport.LinguisticIntercultural.TravellingLivingAdapter
import com.jafar.portfolia.database.MainDatabase
import com.jafar.portfolia.database.PassportEntity.*
import com.jafar.portfolia.repository.PassportRepository
import com.jafar.portfolia.util.Function
import com.jafar.portfolia.viewmodel.PassportViewModel
import com.jafar.restaurants.ui.viewmodel.PassportViewModelFactory
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.bottom_sheet_persistent.*
import kotlinx.android.synthetic.main.fragment_linguistic.*
import kotlinx.android.synthetic.main.fragment_linguistic.add_primary
import kotlinx.android.synthetic.main.fragment_linguistic.back_summary
import kotlinx.android.synthetic.main.fragment_linguistic.bottom_sheet
import java.text.SimpleDateFormat
import java.util.*


class LinguisticFragment : Fragment() {
    lateinit var viewModel: PassportViewModel
    var sheetBehavior: BottomSheetBehavior<*>? = null
    var integer=0
    private val calendar = Calendar.getInstance()
    // Recyclerview1
    lateinit var adapter: LanguageWorkAdapter
    lateinit var recyclerview: RecyclerView
    private var layoutManager: RecyclerView.LayoutManager? = null
    //Recyclerview2
    lateinit var adapter2: StudyTrainingAdapter
    lateinit var recyclerview2: RecyclerView
    private var layoutManager2: RecyclerView.LayoutManager? = null
    // Recyclerview3
    lateinit var adapter3: TravellingLivingAdapter
    lateinit var recyclerview3: RecyclerView
    private var layoutManager3: RecyclerView.LayoutManager? = null
    // for size
    var primary=0
    var secondary=0
    var higher=0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_linguistic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val repository= PassportRepository(MainDatabase(requireActivity()))
        val viewModelProviderFactory = PassportViewModelFactory(requireActivity().application, repository)
        viewModel = ViewModelProvider(this, viewModelProviderFactory).get(PassportViewModel::class.java)

        sheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        back_summary.setOnClickListener {this.findNavController().popBackStack()}
        recyclerview=view.findViewById(R.id.primary_recyclerview9)
        recyclerview2=view.findViewById(R.id.secondary_recyclerview9)
        recyclerview3=view.findViewById(R.id.higher_recyclerview9)
        layoutManager= LinearLayoutManager(activity)
        layoutManager2= LinearLayoutManager(activity)
        layoutManager3= LinearLayoutManager(activity)
        recyclerview.layoutManager = layoutManager
        recyclerview2.layoutManager=layoutManager2
        recyclerview3.layoutManager=layoutManager3
        recyclerview.apply {
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }
        recyclerview2.apply {
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }
        recyclerview3.apply {
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }
        date_of_birth_bottomsheet.setOnClickListener {
            DatePickerDialog(
                requireActivity(), date, calendar[Calendar.YEAR],
                calendar[Calendar.MONTH],
                calendar[Calendar.DAY_OF_MONTH]
            ).show()
        }

        date_started_bottomsheet.setOnClickListener {
            DatePickerDialog(
                requireActivity(), date2, calendar[Calendar.YEAR],
                calendar[Calendar.MONTH],
                calendar[Calendar.DAY_OF_MONTH]
            ).show()
        }


        viewModel.getLanguageWork().observe(viewLifecycleOwner, androidx.lifecycle.Observer {list->
            adapter= LanguageWorkAdapter(requireActivity(),list)
            recyclerview.adapter=adapter
            adapter.notifyDataSetChanged()
            primary=list.size
        })

        viewModel.getLinguisticStudy().observe(viewLifecycleOwner, androidx.lifecycle.Observer { list->
            adapter2= StudyTrainingAdapter(requireActivity(),list)
            recyclerview2.adapter=adapter2
            adapter2.notifyDataSetChanged()
            secondary=list.size
        })
       viewModel.getTravelkLiving().observe(viewLifecycleOwner, androidx.lifecycle.Observer {list->
           adapter3=TravellingLivingAdapter(requireActivity(),list)
           recyclerview3.adapter=adapter3
           adapter3.notifyDataSetChanged()
           higher=list.size
       })


        add_primary.setOnClickListener {
            if (primary<=3){
                BottomSheet()
                integer=1
            }else Function.showToast(requireActivity(),"The number of language should not be more than 4")
        }

        btn_secondary8.setOnClickListener {
            if (secondary<=3) {
                BottomSheet()
                integer = 2
            }else Function.showToast(requireActivity(),"The number of language should not be more than 4")

        }

        btn_higher8.setOnClickListener {
          if (higher<=3){
              BottomSheet()
              integer=3
            }else Function.showToast(requireActivity(),"The number of language should not be more than 4")

        }

        save_bottomsheet.setOnClickListener {
            saveBottomsheet(integer)
        }

    }

    private fun BottomSheet(){
        if ((sheetBehavior as BottomSheetBehavior<*>).state==BottomSheetBehavior.STATE_EXPANDED){
            (sheetBehavior as BottomSheetBehavior<*>).state=BottomSheetBehavior.STATE_COLLAPSED
        }else if ((sheetBehavior as BottomSheetBehavior<*>).state==BottomSheetBehavior.STATE_COLLAPSED){
            (sheetBehavior as BottomSheetBehavior<*>).state=BottomSheetBehavior.STATE_EXPANDED
        }
        language_bottomsheet.setText("")
        details_bottomsheet.setText("")
        date_of_birth_bottomsheet.setText("")
        date_started_bottomsheet.setText("")
    }

    var date =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar[Calendar.YEAR] = year
            calendar[Calendar.MONTH] = monthOfYear
            calendar[Calendar.DAY_OF_MONTH] = dayOfMonth
            setDateOfBirth()
        }

    var date2 =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar[Calendar.YEAR] = year
            calendar[Calendar.MONTH] = monthOfYear
            calendar[Calendar.DAY_OF_MONTH] = dayOfMonth
            setDateStartUsing()
        }

    private fun setDateOfBirth() {
        val myFormat = "dd MMMM yyyy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        date_of_birth_bottomsheet?.setText(sdf.format(calendar.getTime()))
    }

    private fun setDateStartUsing() {
        val myFormat = "dd MMMM yyyy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        date_started_bottomsheet?.setText(sdf.format(calendar.getTime()))
    }

    private fun saveBottomsheet(id:Int){
        if (language_bottomsheet.text.toString().isNotEmpty()&&
            details_bottomsheet.text.toString().isNotEmpty()&&
            date_of_birth_bottomsheet.text.toString().isNotEmpty()&&
            date_started_bottomsheet.text.toString().isNotEmpty()){
            val primaryprimary_edu= Language_work(
                language_bottomsheet.text.toString(),
                details_bottomsheet.text.toString(),
                date_of_birth_bottomsheet.text.toString(),
                date_started_bottomsheet.text.toString())
            val secondary_edu= Linguistic_study(
                language_bottomsheet.text.toString(),
                details_bottomsheet.text.toString(),
                date_of_birth_bottomsheet.text.toString(),
                date_started_bottomsheet.text.toString())
            val higherEdu= Travelling_living(
                language_bottomsheet.text.toString(),
                details_bottomsheet.text.toString(),
                date_of_birth_bottomsheet.text.toString(),
                date_started_bottomsheet.text.toString()
            )
            when(id){
                1-> {
                    Function.showToast(requireActivity(),"Data saved")
                    viewModel.saveLanguageWork(primaryprimary_edu)
                    BottomSheet()
                }
                2-> {
                    Function.showToast(requireActivity(),"Data saved")
                    viewModel.saveLinguisticStudy(secondary_edu)
                    BottomSheet()
                }
                3-> {
                    Function.showToast(requireActivity(),"Data saved")
                    viewModel.saveTravelLiving(higherEdu)
                    BottomSheet()
                }
                else-> {
                    Function.showToast(requireActivity(),"There is no data")}
            }
        }else{
            Function.showToast(requireActivity(),"All filed should be filled")
        }


    }



}