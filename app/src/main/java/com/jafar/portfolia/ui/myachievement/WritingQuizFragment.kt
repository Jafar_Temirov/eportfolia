package com.jafar.portfolia.ui.myachievement

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jafar.portfolia.R
import com.jafar.portfolia.adapter.achieveAdapters.ReadingAdapter
import com.jafar.portfolia.adapter.achieveAdapters.WritingAdapter
import com.jafar.portfolia.database.Entity.ReadingQ
import com.jafar.portfolia.database.Entity.WritingQ
import com.jafar.portfolia.database.MainDatabase
import com.jafar.portfolia.model.Reading
import com.jafar.portfolia.model.Writing2
import com.jafar.portfolia.preference.MyAchievementPreference
import com.jafar.portfolia.repository.PassportRepository
import com.jafar.portfolia.util.Constants
import com.jafar.portfolia.util.Function
import com.jafar.portfolia.util.myachievement.ReadingQuestion
import com.jafar.portfolia.util.myachievement.WritingQuestion
import com.jafar.portfolia.viewmodel.PassportViewModel
import com.jafar.restaurants.ui.viewmodel.PassportViewModelFactory
import java.text.SimpleDateFormat
import java.util.*


class WritingQuizFragment : Fragment(),View.OnClickListener {
    lateinit var viewModel2: PassportViewModel
    lateinit var adapter: WritingAdapter
    lateinit var recyclerview: RecyclerView
    private var layoutManager: RecyclerView.LayoutManager? = null
    var preference: MyAchievementPreference?=null
    var reading: WritingQ?=null
    private val calendar = Calendar.getInstance()
    private var mCurrentPosition: Int = 1
    private var mQuestionsList: ArrayList<Writing2>? = null
    private var mCorrectAnswers: Int = 0
    private var mSelectedOptionPosition: Int = 0
    // TextView option
    var tv_option_one: TextView?=null
    var tv_option_two: TextView?=null
    var tv_option_three: TextView?=null
    var btn_submit: Button?=null
    var next_level: Button?=null
    var tv_question: TextView?=null
    //
    var logo_app_quiz: TextView?=null
    var xay=0
    var tanlov=0
    var davvom_ettir=true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View=inflater.inflate(R.layout.fragment_writing_quiz, container, false)

        val repository2= PassportRepository(MainDatabase(requireActivity()))
        val viewModelProviderFactory2= PassportViewModelFactory(
            requireActivity().application,
            repository2
        )
        viewModel2= ViewModelProvider(this, viewModelProviderFactory2).get(PassportViewModel::class.java)
        recyclerview=view.findViewById(R.id.writing_recyclerview)
        layoutManager= LinearLayoutManager(activity)
        recyclerview.layoutManager = layoutManager
        recyclerview.apply {
            itemAnimator= DefaultItemAnimator()
            isNestedScrollingEnabled=false
            setHasFixedSize(true)
        }

        preference= MyAchievementPreference(requireActivity())
        tv_question=view.findViewById(R.id.tv_question_writing)
        tv_option_one=view.findViewById(R.id.tv_option_one_writing)
        tv_option_two=view.findViewById(R.id.tv_option_two_writing)
        tv_option_three=view.findViewById(R.id.tv_option_three_writing)
        next_level=view.findViewById(R.id.next_level_writing)
        logo_app_quiz=view.findViewById(R.id.writing_app_quiz)
        view.findViewById<ImageView>(R.id.cert_back_writing).setOnClickListener {
            if (preference!!.getWritingQ(Constants.WRITING)!!){
                this.findNavController().popBackStack()
            }else {

                val builder1: AlertDialog.Builder = AlertDialog.Builder(context)
                builder1.setMessage("Do you not want to complete the question list ?")
                builder1.setCancelable(true)
                builder1.setPositiveButton(
                    "Yes"
                ) { dialog, _ ->
                    preference!!.setWriting(Constants.WRITING, false)
                    preference!!.setWriteRate(Constants.WRITING_TITLE, "")
                    viewModel2.deletingWriting()
                    this.findNavController().popBackStack()
                }
                builder1.setNegativeButton(
                    "No"
                ) { dialog, id -> dialog.cancel() }
                val alert11: AlertDialog = builder1.create()
                alert11.show()
            }
        }

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    Log.d("TAG", "Back Pressed")
                    if (preference!!.getWritingQ(Constants.WRITING)!!){
                        //Function.showToast(requireActivity(), "Stay here1!!!!")
                        //requireActivity().onBackPressed()
                        findNavController().navigateUp()
                    }else {

                        val builder1: AlertDialog.Builder = AlertDialog.Builder(context)
                        builder1.setMessage("Do you not want to complete the question list ?")
                        builder1.setCancelable(true)
                        builder1.setPositiveButton(
                            "Yes"
                        ) { dialog, _ ->
                            preference!!.setWriting(Constants.WRITING, false)
                            preference!!.setWriteRate(Constants.WRITING_TITLE, "")
                            viewModel2.deletingWriting()
                            findNavController().navigateUp()
                        }
                        builder1.setNegativeButton(
                            "No"
                        ) { dialog, id -> dialog.cancel() }
                        val alert11: AlertDialog = builder1.create()
                        alert11.show()

                    }
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        view.findViewById<ImageView>(R.id.edit_writing).setOnClickListener {

            val builder1: AlertDialog.Builder = AlertDialog.Builder(context)
            builder1.setMessage("Are you sure you want to delete and upgrade your degree ?")
            builder1.setCancelable(true)
            builder1.setPositiveButton(
                "Yes"
            ) { dialog, _ ->
                preference!!.setWriting(Constants.WRITING, false)
                preference!!.setWriteRate(Constants.WRITING_TITLE, "")
                viewModel2.deletingWriting()
                findNavController().navigateUp()
            }
            builder1.setNegativeButton(
                "No"
            ) { dialog, id -> dialog.cancel() }
            val alert11: AlertDialog = builder1.create()
            alert11.show()
        }

        if (preference!!.getWritingQ(Constants.WRITING)!!){
            view.findViewById<LinearLayout>(R.id.linear_writing).visibility=View.GONE
            view.findViewById<ImageView>(R.id.edit_writing).visibility=View.VISIBLE
            recyclerview.visibility=View.VISIBLE

            viewModel2.getWriting().observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                if (it != null && it.size > 1) {
                    val ff = preference!!.getWriteRate(Constants.WRITING_TITLE)
                    logo_app_quiz!!.text = "WRITING $ff"
                    Log.d("TAG", "Inside ${it.size} + ${it.toString()}")
                    adapter = WritingAdapter(requireActivity(), it)
                    recyclerview.adapter = adapter
                    adapter.notifyDataSetChanged()
                }
            })
        }else{
            logo_app_quiz!!.text="WRITING B1"
            mQuestionsList = WritingQuestion.getWritingQuiz()
            tv_option_one?.setOnClickListener(this)
            tv_option_two?.setOnClickListener(this)
            tv_option_three?.setOnClickListener(this)

            btn_submit =view.findViewById(R.id.btn_submit_writing)
            btn_submit!!.setOnClickListener(this)
            setQuestion()
        }

        return view
    }


    private fun setQuestion() {
        val question = mQuestionsList!!.get(mCurrentPosition - 1) // Getting the question from the list with the help of current position.
        defaultOptionsView()
        if (mCurrentPosition == mQuestionsList!!.size) {
            btn_submit?.text = "Final Question"
        } else {
            btn_submit?.text = "Select"
        }
        // END
        tv_question?.text = question.question
        tv_option_one?.text = question.optionOne
        tv_option_two?.text = question.optionTwo
        tv_option_three?.text = question.optionThree
    }

    private fun defaultOptionsView() {
        tv_option_one?.apply {
            setTextColor(Color.parseColor("#7A8089"))
            typeface = Typeface.DEFAULT
            background = ContextCompat.getDrawable(
                requireActivity(),
                R.drawable.default_option_border_bg
            )
        }
        tv_option_two?.apply {
            setTextColor(Color.parseColor("#7A8089"))
            typeface = Typeface.DEFAULT
            background = ContextCompat.getDrawable(
                requireActivity(),
                R.drawable.default_option_border_bg
            )
        }
        tv_option_three?.apply {
            setTextColor(Color.parseColor("#7A8089"))
            typeface = Typeface.DEFAULT
            background = ContextCompat.getDrawable(
                requireActivity(),
                R.drawable.default_option_border_bg
            )
        }
    }

    private fun selectedOptionView(tv: TextView, selectedOptionNum: Int) {
        defaultOptionsView()
        mSelectedOptionPosition = selectedOptionNum

        tv.setTextColor(
            Color.parseColor("#363A43")
        )
        tv.setTypeface(tv.typeface, Typeface.BOLD)
        tv.background = ContextCompat.getDrawable(
            requireActivity(),
            R.drawable.selected_option_border_bg
        )
        if (selectedOptionNum==3){
            DatePickerDialog(
                requireActivity(), date2, calendar[Calendar.YEAR], calendar[Calendar.MONTH],
                calendar[Calendar.DAY_OF_MONTH]
            ).show()
        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_option_one_writing -> {
                selectedOptionView(tv_option_one!!, 1)
                xay = 1
                tanlov = 1
            }

            R.id.tv_option_two_writing -> {
                selectedOptionView(tv_option_two!!, 2)
                xay = 1
                tanlov = 2
            }

            R.id.tv_option_three_writing -> {
                selectedOptionView(tv_option_three!!, 3)
                xay = 1
                tanlov = 3
            }
            // START
            R.id.btn_submit_writing -> {
                if (xay == 1) {
                    if (mSelectedOptionPosition == 0) {
                        Log.d("Jafar", "bir")
                        xay = 0
                        mCurrentPosition++
                        when {
                            mCurrentPosition <= mQuestionsList!!.size -> {
                                Log.e("DDDDDDD", "Position $mCurrentPosition")
                                Log.e("DDDDDDDD", "PositionList $mCorrectAnswers")
                                if (davvom_ettir) {
                                    var ss = ""
                                    when (tanlov) {
                                        1 -> ss = tv_option_one?.text.toString()
                                        2 -> ss = tv_option_two?.text.toString()
                                        3 -> ss = tv_option_three?.text.toString()
                                        else -> Log.d("Jafar", "No Data")
                                    }
                                    reading = WritingQ(tv_question?.text.toString(), ss)
                                    viewModel2.saveWriting(reading!!)
                                    setQuestion()
                                } else {
                                    var rr = ""
                                    when (mCurrentPosition) {
                                        in 0..10 -> rr = "B1"
                                        in 11..18 -> rr = "B2"
                                        else -> rr = "C1"
                                    }
                                    preference?.setWriteRate(Constants.WRITING_TITLE, rr)
                                    preference?.setWriting(Constants.WRITING, true)
                                    Log.e("Jafar", "exit1")
                                    this.findNavController().popBackStack()
                                }

                            }
                            else -> {
                                preference?.setWriteRate(Constants.WRITING_TITLE, "C1")
                                preference?.setWriting(Constants.WRITING, true)
                                Log.e("Jafar", "exit2")
                                this.findNavController().popBackStack()
                            }
                        }
                    } else {
                        Log.d("Jafar", "ikki")
                        val question = mQuestionsList?.get(mCurrentPosition - 1)
                        if (question!!.correctAnswer == mSelectedOptionPosition) {
                            mCorrectAnswers++
                        }
                        Log.e("JJJJJJJJJ", "Position $mCurrentPosition")
                        Log.e("JJJJJJJJJ", "PositionList $mCorrectAnswers")
                        if (mCurrentPosition >= 9 && mCurrentPosition < 10) {
                            if (mCurrentPosition == 9 && mCorrectAnswers == 9) {
                                Log.d("Jafar", "Next Level")
                                // next_level?.text = "NEXT LEVEL"
                                btn_submit?.text = "NEXT LEVEL"
                                // next_level?.visibility = View.VISIBLE
                                davvom_ettir = true
                            } else {
                                Log.d("Jafar", "Shu yerda")
                                davvom_ettir = false
                            }
                        } else {
                            if (mCurrentPosition >= 17 && mCurrentPosition < 18) {
                                if (mCurrentPosition == 17 && mCorrectAnswers == 17) {
                                    Log.d("Jafar", "Next Level$mCurrentPosition")
                                    // next_level?.text = "NEXT LEVEL"
                                    btn_submit?.text = "NEXT LEVEL"
                                    // next_level?.visibility = View.VISIBLE
                                    davvom_ettir = true
                                } else {
                                    Log.d("Jafar", "Shu yerda22")
                                    davvom_ettir = false
                                }
                            }
                        }

                        if (mCurrentPosition > 9 && mCorrectAnswers > 9) {
                            logo_app_quiz!!.text = "WRITING B2"
                            next_level?.visibility = View.GONE
                        }
                        if (mCurrentPosition > 17 && mCorrectAnswers > 17) {
                            logo_app_quiz!!.text = "WRITING C1"
                            next_level?.visibility = View.GONE
                        }

                        if (mCurrentPosition == mQuestionsList!!.size) {
                            btn_submit?.text = "Finish"
                        } else {
                            btn_submit?.text = "Next question"
                        }

                        mSelectedOptionPosition = 0
                    }

                } else {
                    Function.showToast(requireActivity(), "Please... Select one of them")
                }
            }
        }
    }


    var date2 = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        calendar[Calendar.YEAR] = year
        calendar[Calendar.MONTH] = monthOfYear
        calendar[Calendar.DAY_OF_MONTH] = dayOfMonth
        setDateOfObtained()}
    private fun setDateOfObtained() {
        val myFormat = "dd MMMM yyyy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        tv_option_three?.setText(sdf.format(calendar.getTime()))
    }

}