package com.jafar.portfolia.ui.dialogfragment

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.jafar.portfolia.BuildConfig
import com.jafar.portfolia.R
import com.jafar.portfolia.database.Entity.RegistrationEntity
import com.jafar.portfolia.database.MainDatabase
import com.jafar.portfolia.repository.RegisterRepository
import com.jafar.portfolia.ui.photoFile
import com.jafar.portfolia.util.Constants
import com.jafar.portfolia.util.FileUtil
import com.jafar.portfolia.util.Function
import com.jafar.restaurants.ui.viewmodel.RegisterViewModel
import com.jafar.restaurants.ui.viewmodel.RegisterViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jafar.portfolia.util.Function.Companion.rotateImage
import kotlinx.coroutines.launch
import java.io.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.pow

class MyProfileFragment : DialogFragment() {

    lateinit var viewModel: RegisterViewModel
    var registrationEntity: List<RegistrationEntity>?=null
    val TAG="MyProfileFragment"
    var imageView:ImageView?=null
    var floatactionButton:FloatingActionButton?=null
    var image_uri=""

    var outputStream: OutputStream?=null

    // for files
    private var actualImage1: File? = null
    var selectedImage: Uri?=null
    var currentPhotoPath: String? = null
    var externalstorage:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullscreenDialogtheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val view:View=inflater.inflate(R.layout.fragment_my_profile, container, false)

        val repository= RegisterRepository(MainDatabase(requireActivity()))
        val viewModelProviderFactory = RegisterViewModelFactory(requireActivity().application,repository)
        viewModel = ViewModelProvider(this, viewModelProviderFactory).get(RegisterViewModel::class.java)
        imageView=view.findViewById(R.id.picture_myprofile)
        floatactionButton=view.findViewById(R.id.fabChoosePic_myprofile)
        viewModel.getRegistration().observe(viewLifecycleOwner, androidx.lifecycle.Observer { response ->
                registrationEntity = response
                Log.d(TAG, "Response:" + registrationEntity.toString())
                var ss = ""
                for (i in registrationEntity!!) {
                    ss = i.image_uri
                    view.findViewById<EditText>(R.id.name_myprofile).setText(i.name)
                    view.findViewById<EditText>(R.id.date_of_birth_myprofile).setText(i.date_of_birth)
                    view.findViewById<EditText>(R.id.place_of_birth_myprofile).setText(i.place_of_birth)
                    view.findViewById<EditText>(R.id.nationality_myprofile).setText(i.nationality)
                    view.findViewById<EditText>(R.id.mother_tongue_myprofile).setText(i.mother_tongue)
                    view.findViewById<EditText>(R.id.date_started_myprofile).setText(i.date_using)
                    view.findViewById<EditText>(R.id.date_of_birth_myprofile).setText(i.date_of_birth)
                    val requestOptions = RequestOptions()
                        .placeholder(R.color.card1)
                        .error(R.color.card3)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()

                    Glide.with(requireActivity())
                        .applyDefaultRequestOptions(requestOptions)
                        .load(Uri.parse(ss))
                       .into(imageView!!)
                    image_uri=ss
                }
            })
        floatactionButton!!.setOnClickListener {
            val popupmenu = PopupMenu(requireActivity(), it)
            popupmenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.photo_gallery -> {
                        openImageChooser()
                        true
                    }
                    R.id.photo_take -> {
                         askCameraPermissions();
                        true
                    }

                    else -> false
                }
            }
            popupmenu.inflate(R.menu.popup_menu_photo)
            try {
                val fieldsPopup = PopupMenu::class.java.getDeclaredField("mPopup")
                fieldsPopup.isAccessible = true
                val mPopup = fieldsPopup.get(popupmenu)
                mPopup.javaClass.getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                    .invoke(mPopup, true)
            } catch (e: Exception) {
                Log.d("TAG", "$e")
            } finally {
                popupmenu.show()
            }
        }
        view.findViewById<View>(R.id.edit_profile).setOnClickListener {
          if (floatactionButton!!.visibility==View.GONE){
              floatactionButton!!.visibility=View.VISIBLE
              view.findViewById<EditText>(R.id.name_myprofile).isEnabled=true
              view.findViewById<EditText>(R.id.date_of_birth_myprofile).isEnabled=true
              view.findViewById<EditText>(R.id.place_of_birth_myprofile).isEnabled=true
              view.findViewById<EditText>(R.id.nationality_myprofile).isEnabled=true
              view.findViewById<EditText>(R.id.mother_tongue_myprofile).isEnabled=true
              view.findViewById<EditText>(R.id.date_started_myprofile).isEnabled=true
              view.findViewById<EditText>(R.id.date_of_birth_myprofile).isEnabled=true
              view.findViewById<View>(R.id.save_reg_myprofile).isEnabled=true
          }else {
              floatactionButton!!.visibility=View.GONE
              view.findViewById<EditText>(R.id.name_myprofile).isEnabled=false
              view.findViewById<EditText>(R.id.date_of_birth_myprofile).isEnabled=false
              view.findViewById<EditText>(R.id.place_of_birth_myprofile).isEnabled=false
              view.findViewById<EditText>(R.id.nationality_myprofile).isEnabled=false
              view.findViewById<EditText>(R.id.mother_tongue_myprofile).isEnabled=false
              view.findViewById<EditText>(R.id.date_started_myprofile).isEnabled=false
              view.findViewById<EditText>(R.id.date_of_birth_myprofile).isEnabled=false
              view.findViewById<View>(R.id.save_reg_myprofile).isEnabled=false
              view.findViewById<View>(R.id.save_reg_myprofile).isFocusable=false

          }

            view.findViewById<View>(R.id.save_reg_myprofile).setOnClickListener {
                saveImageToStorage()
                if (externalstorage==null){
                   externalstorage=image_uri
                }
                val registrationEntity=RegistrationEntity(
                    view.findViewById<EditText>(R.id.name_myprofile).text.toString(),
                    view.findViewById<EditText>(R.id.date_of_birth_myprofile).text.toString(),
                    view.findViewById<EditText>(R.id.place_of_birth_myprofile).text.toString(),
                    view.findViewById<EditText>(R.id.nationality_myprofile).text.toString(),
                    view.findViewById<EditText>(R.id.mother_tongue_myprofile).text.toString(),
                    view.findViewById<EditText>(R.id.date_started_myprofile).text.toString(),
                    externalstorage!!)

                viewModel.updateRegister(registrationEntity)
                dismiss()

            }
        }
        view.findViewById<View>(R.id.back_profile).setOnClickListener {dismiss()}
        return view
    }

    private fun openImageChooser(){
        Intent(Intent.ACTION_PICK).also {
            it.type="image/*"
            val mimeTypes= arrayOf("image/jpeg", "image/png", "image/jpg")
            it.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            startActivityForResult(it, Constants.REQUEST_CODE_IMAGE)
        }
    }

    private fun askCameraPermissions() {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.CAMERA),
                Constants.CAMERA_PERM_CODE
            )
            //dispatchTakePictureIntent()
        } else {
            Log.d(TAG, "dispatchTake")
            dispatchTakePictureIntent()
        }
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(requireActivity().packageManager) != null) {
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                Log.e(TAG, ex.toString())
            }
            if (photoFile != null) {
                val photoURI = FileProvider.getUriForFile(
                    requireActivity(),
                    BuildConfig.APPLICATION_ID + ".fileprovider",
                    photoFile
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, Constants.CAMERA_REQUEST_CODE)
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    private fun saveImageToStorage(){
        Log.d(TAG,"saveImageToStorage")
        val drawable: BitmapDrawable =imageView?.drawable as BitmapDrawable
        val bitmap: Bitmap =drawable.bitmap
        val filePath:File=Environment.getExternalStorageDirectory()
        val dir= File(filePath.absolutePath+"/")
        if (!dir.exists()){
            dir.mkdirs()
        }
        val file=File(dir,System.currentTimeMillis().toString()+".jpg")
        try{
            outputStream= FileOutputStream(file)
        }catch (e: FileNotFoundException){
            e.printStackTrace()
            Log.e(TAG,e.localizedMessage)
        }

        val files=File(file.path)
        val usi=Uri.fromFile(files)
        externalstorage=usi.toString()

        bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream)
        outputStream?.flush()
        outputStream?.close()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode== Activity.RESULT_OK ){
            when(requestCode){
                Constants.REQUEST_CODE_IMAGE -> {
                    try {
                        lifecycleScope.launch {
                            selectedImage = data?.data
                            //restoran_logo.setImageURI(selectedImage)
                            actualImage1 = FileUtil.from(requireActivity(), selectedImage)?.also {
                                //profile_image?.setImageURI(selectedImage)
                                Log.d(
                                    TAG, "Before compressing" + String.format(
                                        "Size : %s", getReadableFileSize(
                                            it.length()
                                        )
                                    )
                                )
                                val requestOptions = RequestOptions()
                                    .placeholder(R.color.card1)
                                    .error(R.color.card3)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .centerCrop()

                                Glide.with(requireActivity())
                                    .applyDefaultRequestOptions(requestOptions)
                                    .load(selectedImage)
                                    .into(imageView!!)

                            }
                        }
                    } catch (e: IOException) {
                        Function.showToast(requireActivity(), "Failed to read picture data! $e")
                        e.printStackTrace()
                    }
                }
                Constants.CAMERA_REQUEST_CODE -> {
                    if (resultCode == Activity.RESULT_OK) {
                        val f = File(currentPhotoPath)
                        val ei: ExifInterface? = ExifInterface(currentPhotoPath!!)
                        var orientation: Int = ei!!.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_UNDEFINED
                        )

                        Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f))
                        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
                        val contentUri = Uri.fromFile(f)
                        mediaScanIntent.data = contentUri
                        requireActivity().sendBroadcast(mediaScanIntent)
                        val bitmap= BitmapFactory.decodeFile(photoFile.absolutePath)
                        var rotatedBitmap: Bitmap? = null
                        rotatedBitmap = when (orientation) {
                            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90f)
                            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180f)
                            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270f)
                            ExifInterface.ORIENTATION_NORMAL -> bitmap
                            else -> bitmap
                        }
                        imageView?.setImageBitmap(rotatedBitmap)
                    }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray){
        if(requestCode == Constants.CAMERA_PERM_CODE){
            if(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                dispatchTakePictureIntent();
            }else {
                Toast.makeText(requireActivity(),"Camera Permission is Required to Use camera.",
                    Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode==1001){
            if (grantResults.size>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                saveImageToStorage()
            }else{
                Function.showToast(requireActivity(),"Permission not granted")
            }
        }
    }

    private fun getReadableFileSize(size: Long): String {
        if (size <= 0) {
            return "0"
        }
        val units = arrayOf("B", "KB", "MB", "GB", "TB")
        val digitGroups = (Math.log10(size.toDouble()) / Math.log10(1024.0)).toInt()
        return DecimalFormat("#,##0.#").format(size / 1024.0.pow(digitGroups.toDouble())) + " " + units[digitGroups]
    }

    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        dialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation
    }



}