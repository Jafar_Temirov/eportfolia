package com.jafar.portfolia.model

data class Listening(val id: Int,
                     val question: String,
                     val optionOne: String,
                     val optionTwo: String,
                     val optionThree: String,
                     val correctAnswer: Int){
}