package com.jafar.portfolia.model

data class Writing2(val id: Int,
                    val question: String,
                    val optionOne: String,
                    val optionTwo: String,
                    val optionThree: String,
                    val correctAnswer: Int){
}