package com.jafar.portfolia.model

data class Spoken_production(val id: Int,
                             val question: String,
                             val optionOne: String,
                             val optionTwo: String,
                             val optionThree: String,
                             val correctAnswer: Int){
}