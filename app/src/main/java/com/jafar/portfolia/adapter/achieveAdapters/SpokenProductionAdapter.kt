package com.jafar.portfolia.adapter.achieveAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jafar.portfolia.R
import com.jafar.portfolia.database.Entity.ReadingQ
import com.jafar.portfolia.database.Entity.SpokenProductionQ
import kotlinx.android.synthetic.main.item_reading.view.*
import kotlinx.android.synthetic.main.item_spokenprod.view.*

class SpokenProductionAdapter(var context: Context, var list: List<SpokenProductionQ>)
    : RecyclerView.Adapter<SpokenProductionAdapter.ReadingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReadingViewHolder {
     return ReadingViewHolder(LayoutInflater.from(context).inflate(R.layout.item_spokenprod,parent,false))
    }

    override fun onBindViewHolder(holder: ReadingViewHolder, position: Int) {
     val read:SpokenProductionQ=list[position]
     holder.quiz.text=read.quiz
        val ee="I can do it easily"
        val tt="I can do it with help"
        if (read.answer.equals(ee.trim())||read.answer.equals(tt.trim())){
            holder.answer.text=read.answer
        }else  holder.answer.text="Until "+read.answer+" ,I should be able to do it"

    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ReadingViewHolder(itemview: View):RecyclerView.ViewHolder(itemview){
        val quiz=itemview.spokenprod_quiz
        val answer=itemview.spokenprod_answer
    }

}