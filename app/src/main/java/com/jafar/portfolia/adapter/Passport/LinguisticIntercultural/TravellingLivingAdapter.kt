package com.jafar.portfolia.adapter.Passport.LinguisticIntercultural

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jafar.portfolia.R
import com.jafar.portfolia.database.PassportEntity.Travelling_living
import kotlinx.android.synthetic.main.item_language_learning.view.*

class TravellingLivingAdapter(var context: Context, var list: List<Travelling_living>):
    RecyclerView.Adapter<TravellingLivingAdapter.TravellViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TravellViewHolder {
      return TravellViewHolder(LayoutInflater.from(context).inflate(R.layout.item_language_learning,parent,false))
    }

    override fun onBindViewHolder(holder: TravellViewHolder, position: Int) {
        val primaryEdu:Travelling_living=list[position]
        holder.language.text="Language: "+primaryEdu.language
        holder.details.text="Details: "+primaryEdu.details
        holder.from.text="From: "+primaryEdu.from
        holder.from.text="To:  "+primaryEdu.to
      //  holder.initialize(primaryEdu,clicklidtrnrt)
    }

    override fun getItemCount(): Int {return list.size}

    class TravellViewHolder(itemview:View):RecyclerView.ViewHolder(itemview) {
        val language=itemview.language
        val details=itemview.details
        val from=itemview.from
        val to=itemview.to

    }
}