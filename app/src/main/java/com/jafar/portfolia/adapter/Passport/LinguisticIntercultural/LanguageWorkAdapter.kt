package com.jafar.portfolia.adapter.Passport.LinguisticIntercultural

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jafar.portfolia.R
import com.jafar.portfolia.database.PassportEntity.Language_work
import com.jafar.portfolia.database.PassportEntity.Primary_edu
import kotlinx.android.synthetic.main.item_language_learning.view.*

class LanguageWorkAdapter(var context: Context, var list: List<Language_work>):
    RecyclerView.Adapter<LanguageWorkAdapter.LanguageWorkViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageWorkViewHolder {
      return LanguageWorkViewHolder(LayoutInflater.from(context).inflate(R.layout.item_language_learning,parent,false))
    }

    override fun onBindViewHolder(holder: LanguageWorkViewHolder, position: Int) {
        val primaryEdu:Language_work=list[position]
        holder.language.text="Language: "+primaryEdu.language
        holder.details.text="Details: "+primaryEdu.details
        holder.from.text="From: "+primaryEdu.from
        holder.from.text="To:  "+primaryEdu.to
      //  holder.initialize(primaryEdu,clicklidtrnrt)
    }

    override fun getItemCount(): Int {return list.size}

    class LanguageWorkViewHolder(itemview:View):RecyclerView.ViewHolder(itemview) {
        val language=itemview.language
        val details=itemview.details
        val from=itemview.from
        val to=itemview.to

        fun initialize(item:Primary_edu, action:OnItemClickListener){
            itemView.setOnClickListener { action.onItemClick(item,adapterPosition) } }
    }

    interface OnItemClickListener{
        fun onItemClick(item: Primary_edu, position: Int)
    }

}