package com.jafar.portfolia.adapter.Passport

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jafar.portfolia.R
import com.jafar.portfolia.database.PassportEntity.Primary_edu
import com.jafar.portfolia.database.PassportEntity.higher_edu
import kotlinx.android.synthetic.main.item_language_learning.view.*

class HigherEducationAdapter(var context: Context, var list: List<higher_edu>):
    RecyclerView.Adapter<HigherEducationAdapter.HigherViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HigherViewHolder {
      return HigherViewHolder(LayoutInflater.from(context).inflate(R.layout.item_language_learning,parent,false))
    }

    override fun onBindViewHolder(holder: HigherViewHolder, position: Int) {
        val primaryEdu:higher_edu=list[position]
        holder.language.text="Language: "+primaryEdu.language
        holder.details.text="Details: "+primaryEdu.details
        holder.from.text="From: "+primaryEdu.from
        holder.from.text="To:  "+primaryEdu.to
      //  holder.initialize(primaryEdu,clicklidtrnrt)
    }

    override fun getItemCount(): Int {return list.size}

    class HigherViewHolder(itemview:View):RecyclerView.ViewHolder(itemview) {
        val language=itemview.language
        val details=itemview.details
        val from=itemview.from
        val to=itemview.to

        fun initialize(item:Primary_edu, action:OnItemClickListener){
            itemView.setOnClickListener { action.onItemClick(item,adapterPosition) } }
    }

    interface OnItemClickListener{
        fun onItemClick(item: Primary_edu, position: Int)
    }

}