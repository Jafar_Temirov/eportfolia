package com.jafar.portfolia.repository

import com.jafar.portfolia.database.Entity.*
import com.jafar.portfolia.database.MainDatabase
import com.jafar.portfolia.database.PassportEntity.*

class PassportRepository(val db: MainDatabase) {
    // reading
    fun getReading()=db.getPassport().getReading()

    suspend fun insertReading(readingQ: ReadingQ)=db.getPassport().insertReading(readingQ)

    suspend fun deleteReading()=db.getPassport().deleteReading()
  // Listening
    fun getListening()=db.getPassport().getListening()

    suspend fun insertListening(listeningQ: ListeningQ)=db.getPassport().insertListening(listeningQ)

    suspend fun deleteListening()=db.getPassport().deleteListening()
  // writing
    fun getWritingQ()=db.getPassport().getWriting()

    suspend fun insertWriting(writingQ: WritingQ)=db.getPassport().insertWriting(writingQ)

    suspend fun deleteWriting()=db.getPassport().deleteWriting()
   // Spoken interaction
    fun getSpokenInteraction()=db.getPassport().getSpokenInter()

    suspend fun insertSpokenInteraction(spokenInterQ: SpokenInterQ)=db.getPassport().insertSpokenInteraction(spokenInterQ)

    suspend fun deleteSpokenInteraction()=db.getPassport().deleteSpokenInter()
   // Spoken production
    fun getSpokenProdiction()=db.getPassport().getSpokenProd()

    suspend fun insertSpokenProduction(spokenProductionQ: SpokenProductionQ)=db.getPassport().insertSpokenProduction(spokenProductionQ)

    suspend fun deleteSpokenProduction()=db.getPassport().deleteSpokenProd()


    fun getPrimary_edu()=db.getPassport().getPrimary_edu()

    suspend fun insertPrimary_edu(primaryEdu: Primary_edu)=db.getPassport().insertPrimary_edu(primaryEdu)

    fun getSecondary_edu()=db.getPassport().getSecond_edu()

    suspend fun insertSecondary_edu(secondaryEdu: Secondary_edu)=db.getPassport().insertSecond_edu(secondaryEdu)

    fun getHigher_edu()=db.getPassport().getHigher_edu()

    suspend fun insertHigher_edu(higherEdu: higher_edu)=db.getPassport().insertHigher_edu(higherEdu)

    // Extra fragment
    fun getLanguageWork()=db.getPassport().getLanguage_work()

    suspend fun insertLanguageWork(languageWork: Language_work)=
        db.getPassport().insertLanguage_work(languageWork)

    fun getLinguisticStudy()=db.getPassport().getLinguisticStudy()

    suspend fun insertLinguistic_study(linguisticStudy: Linguistic_study)=
        db.getPassport().insertLinguistic_study(linguisticStudy)

    fun getTravvelLiving()=db.getPassport().getTravellingLiving()

    suspend fun insertTravellingLiving(travellingLiving: Travelling_living)=
        db.getPassport().insertTravellingLiving(travellingLiving)


}