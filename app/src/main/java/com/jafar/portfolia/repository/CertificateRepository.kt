package com.jafar.portfolia.repository

import com.jafar.portfolia.database.Entity.Certificate
import com.jafar.portfolia.database.MainDatabase

class CertificateRepository(val db:MainDatabase) {

    suspend fun  insertCertificate(certificate: Certificate)=db.getRegister().insertCertificate(certificate)

    fun getCertificate()=db.getRegister().getCertificate()

}