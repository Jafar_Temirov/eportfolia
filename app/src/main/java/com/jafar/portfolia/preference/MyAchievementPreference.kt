package com.jafar.portfolia.preference

import android.content.Context
import com.jafar.portfolia.R

class MyAchievementPreference(context: Context) {
    private val sharedPreference=context.getSharedPreferences(R.string.achieve_file.toString(),Context.MODE_PRIVATE)
   // reading
    fun setReading(key:String,boolean: Boolean){
        sharedPreference.edit().putBoolean(key,boolean).apply()
    }

    fun getReading(key: String):Boolean?{return sharedPreference.getBoolean(key,false)}

    fun setReadRate(key:String,string:String){
        sharedPreference.edit().putString(key,string).apply()
    }

    fun getReadRate(key:String):String?{
        return sharedPreference.getString(key,null)
    }
// listening
    fun setListening(key:String,boolean: Boolean){
        sharedPreference.edit().putBoolean(key,boolean).apply()
    }

    fun getListening(key: String):Boolean?{return sharedPreference.getBoolean(key,false)}

    fun setListenRate(key:String,string:String){
        sharedPreference.edit().putString(key,string).apply()
    }

    fun getListenRate(key:String):String?{
        return sharedPreference.getString(key,null)
    }
    // writing
    fun setWriting(key:String,boolean: Boolean){
        sharedPreference.edit().putBoolean(key,boolean).apply()
    }

    fun getWritingQ(key: String):Boolean?{return sharedPreference.getBoolean(key,false)}

    fun setWriteRate(key:String,string:String){
        sharedPreference.edit().putString(key,string).apply()
    }

    fun getWriteRate(key:String):String?{
        return sharedPreference.getString(key,null)
    }
    // spoken interaction

    fun setSpokenInteraction(key:String,boolean: Boolean){
        sharedPreference.edit().putBoolean(key,boolean).apply()
    }

    fun getSpokenInteraction(key: String):Boolean?{return sharedPreference.getBoolean(key,false)}

    fun setSpokenInterRate(key:String,string:String){
        sharedPreference.edit().putString(key,string).apply()
    }

    fun getSpokenInterRate(key:String):String?{
        return sharedPreference.getString(key,null)
    }

// spoken production
    fun setSpokenProduction(key:String,boolean: Boolean){
        sharedPreference.edit().putBoolean(key,boolean).apply()
    }

    fun getSpokenProduction(key: String):Boolean?{return sharedPreference.getBoolean(key,false)}


    fun setSpokenProdRate(key:String,string:String){
        sharedPreference.edit().putString(key,string).apply()
    }

    fun getSpokenProdRate(key:String):String?{
        return sharedPreference.getString(key,null)
    }

}